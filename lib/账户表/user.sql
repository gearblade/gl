-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 2017-03-26 06:10:28
-- 服务器版本： 5.7.15-1
-- PHP Version: 7.0.12-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nkc`
--

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '学号',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '10',
  `password_reset_token` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `username`, `user_id`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `status`, `password_reset_token`) VALUES
(1, 'xq1024', '201321956', '584486049@qq.com', '$2y$12$cMgjIi.Rm8twuu.hjKlWi.vpm70Pwb9ncjcH6O2mX1XgwNK1OtbP6', 'RMWZq873-FubXDEQo-DkL1rVKkIULcXt', 1465845350, NULL, NULL, '127.0.0.1', 1465838094, 1471550598, 0, 9, ''),
(2, 'admin', '', 'zxq@gearblade.com', '$2y$10$9F9F/1X3kmcfg0A5gjflKejp3EhoZ5n7vJm2cKeTjmSCh/J/AEV8m', 'WnKrmaI64d-fmuMC1CN0mu9PzOeeISqw', 1466453289, NULL, NULL, '127.0.0.1', 1465844804, 1465844804, 0, 10, ''),
(3, '9527', '', '9527@gearblade.com', '$2y$10$6avNH2oWbgtitnC5BC1QFe/8/fyWvUt6j0ZEpzpxTOVLi56V7Ny42', '_YAGCoE-CxHY_FcMJj62rzJuCopu5TeT', 1467459241, NULL, NULL, '127.0.0.1', 1467459242, 1467459242, 0, 10, ''),
(4, 'ccc', '', 'cc@cc.cc', '$2y$12$ZNSpS8Q2FNSPTJwxWReDw..D.zGgC6GNxtvr6BFrQuFT/YfF4IcjS', 'FNtqBC0WmxxcpxBQeq7FSlVmmLxHWNlB', 1469101335, NULL, NULL, '127.0.0.1', 1469101335, 1469101335, 0, 10, ''),
(5, 'gb11', '', 'cc@ww.com', '$2y$12$lAXtv5C9wWoGLP6VavgU9OTdvZsZkmtcwGO5wIBbzGlH1PLcz77IG', 'BMht7c6v7eqWopygKpHCItFcvRB93PGj', NULL, NULL, NULL, '::1', 1471930543, 1471930543, 0, 10, ''),
(6, 'XQ10243243', '', 'zxqc@gearblade.com', '$2y$12$jbWVPGFrsDac1jC8uxeZY.2Si4K8oCLX4bXaoRL8pebautBoC4ZJi', 'hEHGlb32jQ6DDb1ZXTqSglX7tdFLSiKM', NULL, NULL, NULL, '127.0.0.1', 1472290564, 1472290564, 0, 10, ''),
(7, 'erw', '', 'zsxq@gearblade.com', '$2y$12$0MM/YzOgrjdCahQXmGy.rOFOSuE1Z1zXDLPDULTUMyQCASznm0yd2', 'QB8O1JW2OjW6FFpjFV-bIYsNuq28zJLF', NULL, NULL, NULL, '127.0.0.1', 1472291199, 1472291199, 0, 10, ''),
(8, 'erw@cc', '', 'z4sxq@gearblade.com', '$2y$12$.dE0u7kynGbpHCrqnGiLQeijGLKi6m2TrmKCgBejcO6bnHnOgcJgq', 'yX7wxg5UxGntodDlGBGm_NiOOY450dpv', NULL, NULL, NULL, '127.0.0.1', 1472292345, 1472292345, 0, 10, ''),
(9, '啊啊', '', 'sscc@ww.com', '$2y$12$WKTUaI0SPv4gN96ZRJmbweGUurEulKHHqigL1LJWSRdWHWUH29kYq', '3kNtT_z10aQgMcn0KVeAGWPnnMm4pct2', NULL, NULL, NULL, '127.0.0.1', 1472293972, 1472293972, 0, 10, ''),
(10, 'ak48', 'sdfsdf', '', '$2y$12$f0IzKsmbvJueTjKecEcdMOFVaB2cB3H9GnvsHuYODd1NemppY7t/i', 'RaVG2fkmWPOgB46VHnN0WnkqaOAmh5gB', 1472717496, NULL, NULL, NULL, 0, 0, 0, 10, ''),
(11, '333', '333', NULL, '$2y$12$gZ82SaVpyccZFlSbNdJFp.aV4J3G7wFBa/urhPsUaxY88YgDnfv2q', '0DBXY9xvUFb9lQvk6cYSUvCr0VRXtPPv', 1472722898, NULL, NULL, NULL, 0, 0, 0, 10, ''),
(31, '竹纤维', '20132195', NULL, '$2y$12$ZMgySOeJ5RlPm3wtTa3OxuF09LLVF2kf0szp/aU.VMp3YBneKKD3e', 'RDwkgj51xJtaXyisXUrG0fzCu4yyykY-', 1490478672, NULL, NULL, NULL, 1490478672, 1490478672, 0, 10, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
