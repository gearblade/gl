-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 2017-03-26 06:10:38
-- 服务器版本： 5.7.15-1
-- PHP Version: 7.0.12-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nkc`
--

-- --------------------------------------------------------

--
-- 表的结构 `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` text COLLATE utf8_unicode_ci COMMENT '头像',
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction` text COLLATE utf8_unicode_ci COMMENT '简介',
  `room` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '宿舍号',
  `weibo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '微博',
  `bio` text COLLATE utf8_unicode_ci COMMENT '签名档',
  `school` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `school_part` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `introduction`, `room`, `weibo`, `bio`, `school`, `school_part`) VALUES
(1, '哈秋', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAACUCAYAAAB1PADUAAAOd0lEQVR4Xu2de3AdVR3Hv7+zm5SmGEqVhtCWUhqowlArdayU3NtWxULuDeVlxcdIQ/ExOqLOKDNaHwwCDj5mKqPDVKRFZpQBlQK5G6yI1r0plZfF2upYk7bxQZNAabFNTZO75+ds2iogNPfebHZ/Z3f373N+5/v7/j53771nz55DSK/UgQAdoABjpaFSB5AClUIQqAMpUIHamQZLgUoZCNSBFKhA7UyDpUClDATqQAoUgPlruMY+FVMsGyd4EzD4xHvQDxAH6nRCgiUaqMXt/CZt6ZXM+BoBE4/VnIGde0id09VChxPCQWBpJhKouffwpMlT9GdBuBGA/dpu8qNuzn5vYE4nJFCygGKm5g58WEF/F8DJx6sxA8Mv1qkp25fQwYSwEEiaiQFq8XqerGu9+wG6qGznmFe4eftHZbdPGyZjpry5wJ8k0qsJqKmk5qz58mKr/WAlfZLeNuZ3KKas4zkMuphQ8Yen181ZjUkHpNL8YwvUhQ/xaZatnwQwrVJT/PZMWFVssW6tpm+S+8QSqExheClItVf6FfeyaYOhYk6dkM5FVf7RiB1QzR2ljyimMf6QTqcMKkfpSI/4AMVMGUd/kQi3VGvGsX7ugLKxnLyxxkli/3gAFSBMDOop5tQZSYQhiJxjAVS24N0MwqogDFFQjRtz1BtErCTGMB6oZse7XgH+zHcQ1z43Z00JIlBSYxgNVLZQWgmiHwZVPE9zblOr3RFUvCTGMRaozEPDF5KtOgMsmqfq1GQ9gBnM3hwQNYIwmQALrAe1sl8kDz2wsLXYQs8HOG6sQhkJVKaDTwHr3QTUBVgNjwFVzow6A/0E2s6g7xdz9PMANRgfykygHM+HaaYA9/1FeH2scUux1fqeAD2RSzAOqIyj1xP4sside5kAHnlSgy6vpOZvWkYHJGkLW4tRQDW3l9pI0V3lfC2FbaQ/HgP/sqDmJHnawRigLrifp9h1eg8RaqOApdwxGdhTHFAzkjrTbghQTJmC9ygRvbvcwkbaLsEL84wAKuOUlhHImIVuDLxo1alpG5fQYKRgRzC4eKDm388nTZqkewCcFIE/VQ/JwA3FnPWtqgMY2lE8UNkO72ZwMM/pQq6Rd4BU45aETYKKBmrBQ9xQa+s9Uv/VjQoo82b3aasZN5IetW1MGogGKuuU7gNoucleM+GTxRbrDpNzqES7WKAWPnBgqj2hrq+SZIS2Za+kpm9aRs8J1ReoLLFAZRzdTuB8oNlGFIyBfxefUicm4atPLFBZx39WG5+LGc8V81ZVb+CY5IJIoDId3u3E+LRJRpalldDltlhnldXW0EYigYrb3emVbPBTewes5u3LachQZo4rWxxQzQV+jyL9aBzN/l9OtGXvAGW3L4/fRhzigMoWvF4QGuINlL8ygbppmJa4l9Hf45SrKKD8neQmTdeHXn/PpjhZf2S5C8N7b2eu9om4ZCYKqEy7dwMp3BYXc8vMw/83+yW3Rd0GMn8bRllAOd5fCDi7zELErBm5+21q2bqUBkxOTA5QzCrbMfJ1N8FkQ8eknXFQM1/f2WqvG1OcCDuLASrjlFYQyFgjg6rh0fXp24cH1KLNy+nFoOKGFUcMUFlHPwbwu8JKXPo4/h6fBH7AHbA+ZNJyYkFAef+odnMw6XCMVR8zO8VD1jITwBIB1NFVmfvHanyc+zNh2OOhsx7PTfRXr4q9RACV6eCriPVPxbokRxjzoJpWvJL2yJH0SiUygHK8Owj4hFSTZOmiLW5OnS9L0//UCAGq1EmgC6WaJE2XqlNv2Ch0Q34RQGUd/RzA6RbO5ZLLapmbp4fLbR5mu8iBWtDB9bWs9xv7IkKY1To6ln/YUTFv3RTB0KMOGTlQzQ4vUNC/G1Vp2uC/DmgPn+m81LpdoiWRA5Ut8DUgfbdEc6RqGi6pszYvoy6J+iIHKlPwbiLCVySaI1XTQIOqfebtNCxRX/RAOaW1BGqTaI5ETezxVcVLbbG75kUOVLZDPwzmVonFE6ZpUHv8vs5L7YIwXa+QEzlQGUd3EtiQOSg6zIS9BLwA1qE8KvI34met13a21myUDNIxbZEDlXX0VoDPM8CsPf8aUuc8ezmFApIBfrymRAlA7QR4lgEG3uzmrPTPwyiFEgCUGctWmHlDMW9fbAD4kUqMHKiM4/UTcEqkLpQ3OHtKvWXTJfSX8pons1XkQGUL3j7/xAJD7N8xPKDmbV5O/zZEb+gyIwcq43j+vyZzDuwh3uA+abUkYSeVamgUAJTuIvDsasRH1ofVl908jfmgx8j0j+PA0QNVKBWJqHkccxyP0KxZNXXmaed4BDc5ZuRAZR3vPgBGbnvo5qzI/ZMGX+SGZB3v6wC+LM2Y8vTQLjenziyvbTJaRQ5Upr10GSlab6zdhG+7LdYXjNUfsPDogTpy9l2fqSs2maGV5b3zt5fUPhVwbYwMFzlQvmsZx+sh4HQjHfRFM/YODqk5T15Be43NISDhQoDSDxJ4WUA5RRKGwU8Uc9YFgPlb8ozFQBFALSp4H2PCmrEkIqEvM24t5q1VErREpUEEUPPW75tcX1v/AvwDo82+PGZeVszbjtlpVK9eBFC+/KyjdwAchy2XXyrZ6uzHl1J/9WUxt6cgoDz/EOhPmWvly5XTs24LnR+HLQ4rrYcYoDKGTx+82nhm/fVivuarlRbE9PZigDo6fbCbgJmmm3pMP2s1v9hKv49LPuXkIQqobMFbBcLN5Qg3pY0aUidvTNA6dFFAAUxZR/tHVtimAFOGzl1uzkrM8z5hQPn/9rytAEx4C6YMlo40YeAbxZz1pbI7GNxQHlAbeAZK+m8Ge/o60vUiN1fjxi+vV2YkDqgjc1Ke/+6bUaegjwoK4eBAjZr+zEX00qhtDW4gEqjFDs/T0FsM9vX1pG9zc1asvs5fnahIoI5OIQwQUBc3qJj16mK+5nNxy+tYPnKB6hheRKyMeJ+/Qjg8Jr6q2GI/WGE/I5qLBQr3s5Wt0y8Y9M5e+QX3z3SBOr0zT/vK72RGS7lA+QvvHuG5pPUfzLCyYpXPujnrbRX3Et5BNFAjv6UK3tNEmC/cx6rkMWN1MW/F6veUeKAu2MBvrinpbTFYK/Wa0GlWF3Xm6VdVESmwk3igjt6lbiFCXGeah4e1mrW5lf4pkI+KJRkB1NwNPOmkEu8k8NSKMzSiA21TdfS2jUuoZITc44g0Aqgj81J8JUH/zHTDj6P/djdnfcb0/IwA6ox1+yYrGsrVT5z4HSJqMN3019N/8NDgN7X2DhCrfUPk/eZv1zb+ybRcRQPVdPtfJ+AN9T8GcCmAGtPMHYte/6hYANtKlvX+no+86c9jiRVmX7FAjcB0Yv02EJrCNETgWANae4t3rjztaYHa/k+SXKDu7v8ZmK80wcTx1sjAPg+HTtvdNmtwvMcaa3y5QK3r82/56XXUASZc072i4R7phogEqmnd84sB/Rvp5oWs75ddbQ1LQx6z4uFEAjV7bd8NRLit4mzi3WFHV1vDHOkpigRq1tr+qyzi9FDrl9HDwKbutgbxW0eKBMp/+6VpXb+W/mkMVx+t6mqbemu4Y1Y+mlCggKa1vX8C0VsqTymGPZgPwys1dX10+j+kZycWqFl39jVYFveAaIJ0E8dbH7Ne0X1t44/Ge5wg4osFyk9uxt19s2uZf0WgM4JI1rgYzIdZ8ce6VzSKny445q1ooEZEruGaM+3nFyiLP8iMObZlTbMs62wwy9deHcF6uDRc0Mz3DqrSo/+8ZrpR2ywaU5SmDp7QyNwB5sVEUNXVyoxezBgi4CY3bxl3WoMRQDU/ws1Ka39VY2J+T408HGbstg6puRuX00EzPgqAeKAyjncnAdeZYug46GSt1Ls6LyEjXikTC9S89Ty5vlb/EcD0cSiSeSEJq92D6vNYTp5k8SKBWtDB0yew3h3XFxOqBYKJHrMmUn7jEhK76kAcUAvb+QJb6cerNT0B/Xbtt9V5W5fSgMRcRQG10OGsDf3r9M40CiqMvoFT1Yxn3k7D0qASA1TmF3w+SrqTCBOlmSRRDwM9xZwlbsJXBFD+a1KTS7obQGxfQBgXKIkedluUqCNNRACVccw/62VcgBn1mw+ama/uzNtilvpEDlSmnd8B0pvjPvs9bsAxet281Thu8SsMHDlQizq8e5lxdYW60+ZHHRiZUdf6omJrzWMSTIkcqKyjewA296w8AVXUwF2dOUvE04TIgco43ksE1Auoi7ESGPRgMacul5CABKD842FjuglGaCX+iZuzPhTaaMcZKHqgCt42IpwrwQxTNUjaWD9yoLKF0s9BdIWpxRShm/RSt6XmlxK0RA5Us1P6gAL9RIIZRmpgHHRzql7K2XyRA+W/MrXwoaEtJc97q5EFjVI0EZdKun3/oUNf2HnNKTuilHJs7EiBGnmhU/E3wInfYSUIFnaB8JWuFQ3+9keRXZEBNXtd/2oCXw8DVo1GVp3KB2YGftDd1vCJyrsG0yMSoGave+5igvVIMCmkUV7tgMf6fbuubYxk+8jwgbqR7dkz+w8QcEKKwvg4wODD3T1763Hjuf5hlqFeoQM1887eWTU27Qw1ywQOxh6f133dqf7+7qFeoQM1686+uZaNuB63EWrxjjeYB8ruaptaDFtQ6EA1rd17Dqi0PexEEzeexsKulQ2bw847dKBm39E7lU6gvrATTdp4zPbp3de+8e9h5x06UH6CTev6HwH44rCTTcp4DPy6u63h3VHkGwlQM9fsbqyZMHEHGCdGkXTMxxzwPO/cXded1hNFnpEANXKXunvPuWC1AcC0KBKP45gM9HqgS3a3TX02qvwiA8pPuOGe3kl1w7haKfVxAuYBnKjTEgIpOqPEhK1grHmJ1b0vrDzlQCBxqwwSKVBVak67CXYgBUpwcUyUlgJlYtUEa06BElwcE6WlQJlYNcGaU6AEF8dEaSlQJlZNsOYUKMHFMVFaCpSJVROs+T/iHWDR/RqyegAAAABJRU5ErkJggg==', '49651809bd844b4cf775514598dd4da8', '4234', '我就是RMB玩家——秃瓢王富贵', '14ssss', 'http://www.gearblade.com', '祝你身体健康 ，工作顺利，哈哈哈', '西南交通大学', '西南交大_犀浦'),
(2, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', ''),
(3, '王二锤', '9527@gearblade.cc', '/images/upload/160809/5070540500.png', 'ce798135265e7111aa6a6748c5d11461', '日本，东京，原宿', '', '', '', 'keanu', '西南交通大学', ''),
(4, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', ''),
(5, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', ''),
(6, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', ''),
(7, NULL, NULL, '/images/upload/160809/5070540500.png', NULL, NULL, '', '', NULL, NULL, '', ''),
(8, NULL, NULL, '/images/upload/160809/5070540500.png', NULL, NULL, '', '', NULL, NULL, '', ''),
(31, NULL, NULL, '/images/icon/default_m.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- 限制导出的表
--

--
-- 限制表 `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
