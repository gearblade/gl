-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2017-03-22 20:37:02
-- 服务器版本： 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gl`
--

-- --------------------------------------------------------

--
-- 表的结构 `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `title` varchar(80) CHARACTER SET utf8 NOT NULL,
  `content` varchar(8000) CHARACTER SET utf8 NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `level` int(11) DEFAULT '0',
  `uri` varchar(800) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `information`
--

INSERT INTO `information` (`id`, `title`, `content`, `create_time`, `level`, `uri`) VALUES
(2, '第八届全国大学生机械创新设计大赛校内选拔赛通知', '一、 比赛简介：\r\n\r\n全国大学生机械创新大赛是经教育部高等教育司批准，由教育部高等学校机械学科教学指导委员会主办，机械基础课程教学指导分委员会、全国机械原理教学研究会、全国机械设计教学研究会、北京中教仪科技有限公司联合著名高校共同承办，面向大学生的群众性科技活动。目的在于综合设计能力与协作精神；加强学生动手能力的培养和工程实践的训练，提高学生针对实际需求进行机械创新、设计、制作的实践工作能力、吸引、鼓励广大学生踊跃参加课外科技活动，为优秀人才脱颖而出创造条件。过去七届赛事西南交通大学在该项赛事上成绩一直名列全国前列、西南地区第一。\r\n\r\n二、大赛主题：\r\n\r\n根据《第八届全国大学生机械创新设计大赛主题与内容的通知》（第1号通知）精神，为帮助各赛区和参赛者准确理解竞赛的要求，现将有关事项通知如下：\r\n\r\n一、 第八届全国大学生机械创新设计大赛（2018年）的主题为“关注民生、美好家园”，内容为“1）解决城市小区中家庭用车停车难问题的小型停车机械装置的设计与制作；2）辅助人工采摘包括苹果、柑桔、草莓等10种水果的小型机械装置或工具的设计与制作”。\r\n\r\n\r\n为了公平、公正地评审和便于演示，本届大赛设计内容中，家庭用车指小轿车、摩托车、电动车、自行车4种；辅助人工采摘的水果仅针对苹果、梨、桃、枣、柑子、桔子、荔枝、樱桃、菠萝、草莓这10种水果。所有参加决赛的作品必须与本届大赛的主题和内容相符，与主题和内容不符或限定范围不符的作品不能参赛。\r\n\r\n智慧城市、智慧社会是目前发展的主旋律，而服务则是其核心。根据参赛大学生的特点，结合机械科学与工程的发展，本届大赛针对城市小区停车难的问题，开展小型停车装置的机械创新，重点考察学生方法与装置的创新性，包括以节约场地、节约能源、低成本、免维护等科学的停车方案与机械装置。停放位置可以是车主自有场地，也可以是小区公共场所。设计中追求的目标是空间利用率高、安全、便捷。\r\n\r\n在广大的乡村，农业生产广泛采取多种经营，经济作物特别是水果的大量生产和投放市场，丰富了人民的膳食品种，提高了人民的生活质量。全国很多地区在水果的采摘上依然主要靠人工，本届大赛针对量产水果采摘中存在的劳动工作量大、作业范围广（果实分布高低不均）、触碰力度控制要求高（多汁水果易碰伤）以及需选择性采摘（单果成熟期不一致）等问题，展开小型辅助人工采摘机械装置或工具的创新设计与制作。主要目标是提高水果采摘效率、降低劳动强度和采摘成本，保障水果成品质量。\r\n\r\n设计时应注重综合运用所学“机械原理”、“机械设计”等课程的设计原理与方法，注重作品原理、功能、结构上的创新性。\r\n\r\n参赛作品必须以机械设计为主，提倡采用先进理论和先进技术，如机电一体化技术等。对作品的评价不以机械结构为单一标准，而是对作品的功能、设计、结构、工艺制作、性能价格比、先进性、创新性、实用性等多方面进行综合评价。在实现功能相同的条件下，机械结构越简单越好。', '2017-03-22 09:12:09', 0, 'http://dean.swjtu.edu.cn/servlet/NewsView?NewsID=461E9944250AE885'),
(3, '关于成都校区本科生大学英语课程学分替代的说明', '各位同学：\r\n\r\n依据教育教学的发展及学校的具体情况，自2014级开始学校对英语基础语课程体系（除艺术类与外语类专业外）进行了调整，因同学当年未修或未修过，考虑到人数与课程性质，后续此类专业学生重修不再单独开班，采用课程替代方式，经与外国语学院商议，具体方案如下。', '2017-03-22 11:39:44', 0, 'http://dean.swjtu.edu.cn/servlet/NewsView?NewsID=02540EC1D9F0DCBF'),
(4, '2017年上半年全国大学外语四、六级考试报名的通知', '根据教育部考试中心和四川省教育考试院相关文件精神，现将2017年上半年（春季）全国大学外语四、六级考试报名工作有关事项通知如下：', '2017-03-22 11:40:16', 0, 'http://dean.swjtu.edu.cn/servlet/NewsView?NewsID=677EAD9049B82BDF'),
(5, '三创谷首届“互联网+”大学生创新创业大赛通知', '为了给有新锐的创业思想、优秀的创业思维、高效的执行能力的优秀大学生提供一个挥洒创业激情，结交创业伙伴，提升创业技能，展现创业才华和能力的平台，特举办此次创新创业大赛。 \r\n一、活动名称\r\n    “创新凝聚力量 创业成就未来”——2017三创谷首届“互联网+”大学生创新创业大赛\r\n二、活动背景\r\n    推进大众创业、万众创新，是党中央、国务院在经济新常态下做出的重要战略部署，也是实施创新驱动发展战略的应有之意。李克强总理在《政府工作报告》中强调，要打造大众创业、万众创新和增加公共产品、公共服务“双引擎”，推动发展调速不减势、量增质更优。本届大赛旨在引导大学生创新创业意识，为创新创业项目提供交流展示的舞台，吸引更多社会关注和促成项目与社会资源有效对接，为优质大学生创新创业项目配套奖励菁蓉镇及华迪·三创谷孵化资源和可持续性的落地发展环境，助推大学生创新创业项目成果转化。\r\n三、组织机构\r\n主办单位：华迪·三创谷大学生创业孵化公共服务平台  郫都区人民政府  成都市创新创业示范基地管委会\r\n支持单位：成都服务贸易协会 成都科技企业孵化器协会 四川省高校计算机基础教育研究会\r\n承办单位：华迪·三创谷大学生创业孵化公共服务平台\r\n四、面向对象\r\n    全国高校在校大学生与毕业5年内的大学生\r\n五、参赛条件\r\n    参赛团队自主选题，要求能够将“互联网+”与国民经济相关领域紧密结合，通过“互联网+”技术引领行业产业服务模式变革与创新，发挥“互联网+”在社会服务中的重要作用，促进“互联网+”与社会生活的各个领域深度融合。', '2017-03-22 11:41:14', 0, 'http://sist.swjtu.edu.cn/news.do?action=showNews&newsId=4384');

-- --------------------------------------------------------

--
-- 表的结构 `link`
--

CREATE TABLE `link` (
  `id` int(11) NOT NULL,
  `title` varchar(80) CHARACTER SET utf8 NOT NULL,
  `icon` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `uri` varchar(800) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `link`
--

INSERT INTO `link` (`id`, `title`, `icon`, `uri`) VALUES
(1, '教务之星', NULL, 'http://dean.swjtu.edu.cn/'),
(2, '扬华素质网', NULL, 'http://www.yanghua.net/'),
(3, '信息科学与技术学院', NULL, 'http://sist.swjtu.edu.cn'),
(4, '体育部', NULL, 'http://sports.swjtu.edu.cn'),
(5, '新青年素质网', NULL, 'http://sztz.swjtu.edu.cn/'),
(6, '蚂蚁PT', NULL, 'https://pt.antsoul.com'),
(7, '交大图书馆', NULL, 'http://www.lib.swjtu.edu.cn/'),
(8, '网络中心', NULL, 'http://service.swjtu.edu.cn'),
(9, 'SWJTU OlineJudge', NULL, 'http://swjtuoj.cn/'),
(10, '中国大学MOOC', NULL, 'http://www.icourses.cn/imooc/');

-- --------------------------------------------------------

--
-- 表的结构 `searchis`
--

CREATE TABLE `searchis` (
  `id` int(11) NOT NULL,
  `title` varchar(80) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `searchis`
--

INSERT INTO `searchis` (`id`, `title`) VALUES
(1, '教务之星'),
(2, '西南交通大学');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchis`
--
ALTER TABLE `searchis`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `information`
--
ALTER TABLE `information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `link`
--
ALTER TABLE `link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `searchis`
--
ALTER TABLE `searchis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
