/**
 * Created by xq on 16-9-21.
 */
$(function() {
    $(".commit").click(function () {

        var box_commit = $(this).parent().parent().next();
        var id = box_commit.attr('data-id');
        var mind_id = box_commit.attr('data-mind');
        var open = box_commit.attr('data-open');
        //alert(open);
        if(open == 0){

            box_commit.load("/post/comment-list?id=" + id + "&mind_id=" + mind_id);
            box_commit.attr('data-open',1);
        }else {

            box_commit.html(null);
            box_commit.attr('data-open',0);
        }

    })
});
