<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
$this->
title = 'Swjtu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="swjtu-index ">
    <div class="row">
        <div class="col-md-12" style="margin-top: 50px;">
            <!-- search form -->
            <form action="swjtu/search" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="搜索..."/>
                    <span class="input-group-btn">
                    <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                    </span>
                </div>
            </form>
            <div>
                <ol style="float:left; ">
                    <li style="float:left; list-style: none; margin-left: -25px; color: #000000;">热门搜索:</li>
                        <?php foreach ( $dataSearchisProvider->
                        getModels() as $list):?>

                        <li style="float:left; list-style: none; margin-left: 20px; color: #ff0000;">
                            <a href="swjtu/search/?search=<?=$list['title']?>"><?=$list['title'] ?></a>
                        </li>
                        
                        <?php endforeach; ?>
                </ol>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 50px;">
            <!-- /.search form -->
            <div class="box box-solid" style="box-shadow: none;background: none">
                <div class="box-body no-padding">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding: 5px;border: 1px solid #dbe3eb;">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                        </ol>
                        <div class="carousel-inner slider-items">
                            <div class="item">
                                <img src="/adminlte/dist/img/library.jpg" alt="First slide">
                                <div class="carousel-caption">
                                    灰雀教务系统
                                </div>
                            </div>
                            <div class="item">
                                <img src="/adminlte/dist/img/turmp.jpg" alt="Second slide">
                                <div class="carousel-caption">
                                    Second Slide
                                </div>
                            </div>
                            <div class="item active">
                                <img src="/adminlte/dist/img/computer.jpg" alt="Third slide">
                                <div class="carousel-caption">
                                    Third Slide
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8" style="margin-top: 50px;">
            <table class="table table-striped">
            <caption style="text-align: left; color: #000000; font-size: 20px;">热门链接</caption>
            <tbody>
                <?php $count = 5; ?>
                <?php foreach ( $dataLinkProvider->
                        getModels() as $list):?>
                <?php if($count==5){ ?>
                    <tr>
                <?php }?>
                    <?php $count = $count-1;?>
                    <td><a href="<?= $list['uri']?>"><?= $list['title']?></a></td>

                <?php if($count == 0){ $count = 5;?>
                    </tr>
                <?php }?>
                    <?php endforeach; ?>
            </tbody>
            </table>
            </div>
        <div class="col-md-4" style="margin-top: 60px;">
            <a style="text-align: left; color: #000000; font-size: 20px;">最新资讯</a>
            <div class="nav-tabs-custom" style="padding: 10px; margin-top: 8px;">
                    <div class="tab-pane active" id="activity">
                        <?php foreach ( $dataInformationProvider->
                        getModels() as $list):?>
                        <div class="post clearfix" style=" margin-top: -5px;">
                            <div>
                                <a style="color: #000000; font-size: 16px;" href="<?= $list['uri']?>"><?=$list['title']?>
                                </a>
                            </div>
                            <div style="width:400px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">
                                <a style="color: #999999;"><?=$list['content']?>
                                </a>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>