<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Mind */

$this->title = 'Create Mind';
$this->params['breadcrumbs'][] = ['label' => 'Minds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mind-create">


    <?php switch($type) {
        case 'team':
            echo $this->render('_team_form', [
                'model' => $model,
                'team' => $team,
            ]);
            break;

        case 'mind':
            echo $this->render('_form', [
                'model' => $model,
            ]);
            break;

        case 'question':
            echo $this->render('_question_form', [
                'model' => $model,
            ]);
    }
     ?>

</div>
