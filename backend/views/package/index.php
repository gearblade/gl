<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '我的书袋';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="package-index" style="">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="row">
        <div class="col-md-12">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'statusView',
            'username',
            'bookname',

            'create_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
</div>