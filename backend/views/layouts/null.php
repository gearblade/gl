<?php
use yii\helpers\Html;
use yii\helpers\Url;

if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}
backend\assets\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/web/adminlte/dist');
$this->registerCssFile('/css/nav_content.css',['depends'=>'backend\assets\AdminLteAsset']);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
</head>
<body class="hold-transition skin-book layout-top-nav">
<?php $this->beginBody() ?>
<div class="wrapper" >

    <?= $this->render(
        'content_nav.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php //} ?>
