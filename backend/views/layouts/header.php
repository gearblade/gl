<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"><img src="/logo_r2.png" alt="logo" style="width: 180px; padding-left: 5px; padding-bottom: 2px; "></span>
    <span class="logo-lg"><img src="/logo_r2.png" alt="logo" style="width: 180px; padding-left: 5px;    padding-bottom: 2px; "></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php //var_dump(Yii::$app->user->isGuest);die(); ?>
        <?php if(!isset(Yii::$app->user->identity->id)) { ?>
            <li class="">
                <a href="/user/login" >
                    登录
                </a>
            </li>
            <li class="dropdown user user-menu">
                <a href="/user/register" >
                    注册
                </a>
            </li>
                <?php }else{
            /*$social = new \vendor\social\chat();
            $social->login();die();*/
            //$this->registerJsFile('/chat/js/domain.js',['depends'=>'backend\assets\AdminLteAsset','position'=>\yii\web\View::POS_END]);

            ?>
            <script>var  userid = '<?= Yii::$app->user->identity->id ?>'  ;</script>

                <!-- Messages: style can be found in dropdown.less-->



                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::$app->user->identity->profile->gravatar_email ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                <ul class="dropdown-menu">

                    <!-- User image -->


                    <li >
                        <div class="box box-widget ">
                            <!-- Add the bg color to the header using any of the bg-* classes -->

                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="/user/1">主页 <span class="pull-right badge bg-aqua">5</span></a></li>
                                    <?php if(\Yii::$app->user->can('图书添加')){
                                        ?><li><a href="/book/admin">后台 </a></li>
                                        <?php
                                    } ?>
                                    <li><a href="/user/settings/profile">设置 </a></li>
                                    <li><?= Html::a(
                                            '退出',
                                            ['/site/logout'],
                                            ['data-method' => 'post', ]
                                        ) ?></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!-- Menu Body -->

                    <!-- Menu Footer-->

                </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
                <?php } ?>

            </ul>
        </div>
    </nav>
</header>
