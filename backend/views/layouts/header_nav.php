<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use backend\widgets\category\CategoryWidget;
/* @var $this \yii\web\View */
/* @var $content string */

?>

<header class="main-header">


    <nav class="navbar navbar-static-top" role="navigation">
        <div class="container" style="padding-right: 15px;padding-left: 15px;">
        <div class="navbar-header">

            <a href="<?=Url::home()?>" class="navbar-brand" ></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <?php
        $menu = new CategoryWidget(['precate_name'=>'document']);
        $menuItemsCenter = $menu->getCate();

        echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav'],
            'encodeLabels' => false,
            'items' => $menuItemsCenter,
        ]);

        ?>
            </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <?php if(Yii::$app->user->isGuest) { ?>
                    <li class="">
                        <a href="/user/login" >
                            登录
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="/register" >
                            注册
                        </a>
                    </li>
                <?php }else{


                    ?>

                <!-- Tasks: style can be found in dropdown.less -->

                <!-- User Account: style can be found in dropdown.less -->

                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    AdminLTE Design Team
                                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Developers
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Sales Department
                                                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 131.148px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </li>
                            <li class="footer"><a href="/social/chat/main">私信</a></li>
                        </ul>
                    </li>

                    <li class="dropdown notifications-menu ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">3</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                                page and may cause design problems
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-red"></i> 5 new members joined
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-light-blue"></i> You changed your username
                                            </a>
                                        </li>
                                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 195.122px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img id="user-avater" src="<?= isset(Yii::$app->user->identity->profile->gravatar_email)?Yii::$app->user->identity->profile->gravatar_email:'/images/icon/default_m.png/images/icon/default_m.png' ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs" id="user-name"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">

                        <li >
                            <div class="box box-widget ">
                                <!-- Add the bg color to the header using any of the bg-* classes -->

                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="/user/profile">主页 <span class="pull-right badge bg-aqua">5</span></a></li>
                                        <?php if(\Yii::$app->user->can('图书添加')){
                                            ?><li><a href="/book/admin">后台 </a></li>
                                        <?php
                                        } ?>
                                        <li><a href="/user/settings/profile">设置 </a></li>
                                        <li><?= Html::a(
                                                '退出',
                                                ['/site/logout'],
                                                ['data-method' => 'post', ]
                                            ) ?></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->

                    </ul>
                </li>
                <?php } ?>
                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
            </div>
    </nav>
   <!-- <div class="breadcrumb_banner" style="">-->
    <div class=" container">
        <a href="<?=Url::home()?>" class="navbar-brand" style="    width: 300px;
            padding: 6px 40px 6px 10px;
                height: auto;
                background: #fff;
                 position: relative">
            <!--<div class="triangle-photo-right"
                 style="
                    border-style: solid;
    border-width: 71px 50px 0 0;
    height: 0;
    position: absolute;
    left: 0px;
    bottom: 0px;
    width: 0;
    z-index: 0;
    border-color: rgb(47, 64, 80) transparent transparent transparent;
                 "></div>-->
            <img src="/logo_r2.png" style="width: 100%;">
            <div class="triangle-photo-right"

               style="
                 border-style: solid;
                     border-width: 0 0 71px 60px;
                 height: 0;
                 position: absolute;
                 right: 0px;
                 top: 0px;
                 width: 0;
                 z-index: 0;
                 border-color: transparent transparent rgb(47, 64, 80) transparent;
                 "></div>
        </a>
            <ul class="panel quick-menu clearfix">

                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=机器人"><i class="fa  fa-bar-chart-o"></i><!--<img class="fa " src="/images/icon/机器人.png">-->机器人</a>
                </li>
                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=交通"><i class="fa  fa-bar-chart-o"></i>交通</a>
                </li>
                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=编程"><i class="fa  fa-bar-chart-o"></i>编程</a>
                </li>
                <li class="col-sm-1 col-xs-3">

                    <a href="/mind/?tag=工程"> <div class="icon">
                            <i class="fa fa-edit"></i>
                        </div>工程</a>
                </li>
                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=理科"><i class="fa  fa-bar-chart-o"></i>数学</a>
                </li>

                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=生化"><i class="fa fa-bell-o"></i>生化</a>
                </li>

                <li class="col-sm-1 col-xs-3">
                    <a href="/mind/?tag=艺术"><i class="fa fa-cogs"></i>艺术</a>
                </li>

            </ul>


        </div>

    <!--</div>-->
</header>
