<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\ueditor\UEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['action'=>'/post/create?id='.$model_id]); ?>
    <?= $form->field($model, 'content' )
        ->widget(UEditor::className(),
            ['class'=>'col-sm-10 ','id'=>'content','name'=>'content',
                'clientOptions'=>[

                    'toolbars'=>[[
                        'fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                    ]]]
            ])
        ->label('回答', ['class' => 'col-sm-2 control-label']) ?>


    <?= $form->field($model, 'unname')->checkbox(['1'=>'0']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
