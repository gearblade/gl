<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<?php foreach ( $list as $v):?>
<div class="box-comment">
    <!-- User image -->
    <img class="img-circle img-sm" src="<?= $v['user']['profile']['gravatar_email']?>"
         alt="User Image">

    <div class="comment-text">
        <div class="">
            <?= $v['user']['username']?>
            <span class="text-muted pull-right">
                <?= Yii::$app->formatter->format($v['create_at'],['datetime','php: Y/m/d '])?>
            </span>
        </div><!-- /.username -->
        <?= $v['content']?>
    </div>
    <!-- /.comment-text -->
</div>
<?php endforeach; ?>

<div class="box-comment">
<div class="post-form">

    <?php $form = ActiveForm::begin(['action'=>'/post/comment?id='.$post_id.'&mind_id='.$mind_id]); ?>

    <div class="img-push input-group" style="margin-bottom: 5px">
    <?= $form->field($model, 'content', ['template' => '{input}'] )
        ->input('text',['class'=>'form-control'])->label(false)
         ?>
        <div class="input-group-btn">
    <?= Html::submitButton($model->isNewRecord ? '评论' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>

