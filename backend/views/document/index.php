<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$cate_id =Yii::$app->request->get('DocumentSearch')['cate'];
$cate = \common\models\Cate::find()->select(['name'])->where(['id'=>$cate_id])->one();

$this->title = $cate->name;
$this->params['breadcrumbs'][] = $this->title;

$this->params['cate'] = Yii::$app->request->get('DocumentSearch')['cate'];
$this->params['cateName'] =  $cate->name;
$this->params['search'] = $this->render('_search', ['model' => $searchModel]);

?>
<div class="document-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('新增', ['create','cate'=>$this->params['cate']], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            //'breviary:ntext',
            //'content:ntext',
            'author',
             'cate',
            // 'type',
            // 'views',
            // 'status',
             'level',
             'create_at',
            // 'pic:ntext',
            // 'create_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
