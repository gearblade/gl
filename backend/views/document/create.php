<?php

use yii\helpers\Html;
use common\models\Cate;

/* @var $this yii\web\View */
/* @var $model common\models\Document */
#$cate= Cate::findOne(Yii::$app->request->get('cate'))? Cate::findOne(Yii::$app->request->get('cate')): Cate::findOne($model->getAttribute('cate'));
$this->params['cate']=$cate;

$this->title = '新建'.$cate['name'].'文档';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index','DocumentSearch[cate]'=>10]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'cate' => $cate,
    ]) ?>

</div>
