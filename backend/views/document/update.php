<?php

use yii\helpers\Html;
use common\models\Cate;
/* @var $this yii\web\View */
/* @var $model common\models\Document */


$this->title = 'Update Document: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => $cate['name'], 'url' => ['index','DocumentSearch[cate]'=>10]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="document-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cate' => $cate,
    ]) ?>

</div>
