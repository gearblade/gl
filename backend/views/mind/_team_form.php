<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $team backend\models\Mind */
/* @var $form yii\widgets\ActiveForm */
$template = '{label}<div class="col-sm-10">{input}{error}{hint}</div>';
$label = ['class' => "col-sm-2 text-c control-label form-label"];
?>
<div class="report-form  row">

    <div class="col-lg-8 ">
        <div class="box box-info">
            <div class="box-header with-border">
                <h4 class="text-center"> 发 起 组 队</h4>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
            <div class="box-body">
                <?= $form->field($model, 'title', ['template' => $template])->textInput(['maxlength' => true,])->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($model, 'content', ['template' => $template])->textarea(['rows' => 6])->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($team, 'race', ['template' => $template])->textInput(['maxlength' => true])->label(null, ['class' => 'col-sm-2 control-label']) ?>
<div class="row">
    <!--<div class="col-sm-6"></div>-->
    <div class="col-sm-4 col-md-offset-2">
        <?= $form->field($team, 'limit_member', ['template' => '{label}{input}{error}{hint}','options' => ['class' => 'form-group form-margin']])
            ->Input('number',['class' => 'form-control'])->label(null, ['class' => ' control-label']) ?>

    </div>
    <div class="col-sm-4 col-md-offset-1">

        <?= $form->field($team, 'already_member', ['template' => '{label}{input}{error}{hint}','options' => ['class' => 'form-group form-margin']])
            ->Input('number')->label(null, ['class' => 'control-label']) ?>

    </div>
</div>

                <?= $form->field($team, 'connect', ['template' => $template])->textarea(['rows' => 6])->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($team, 'orienting', ['template' => $template])->textInput(['maxlength' => true])->label(null, ['class' => 'col-sm-2 control-label']) ?>
                <?= $form->field($model, 'tag', ['template' => $template])->widget('common\widgets\tags\Tags')->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($model, 'img',['template' => $template])->widget('common\widgets\file_upload\FileUpload',[
                    'config'=>[]
                ])->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <div class="form-group">
                </div>
            </div>
            <div class="box-footer">

                <?= Html::submitButton($team->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

