<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MindSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '科创俱乐部';
$this->params['breadcrumbs'][] = $this->title;

$users = new \dektrium\user\models\User();

?>
<div class="mind-index ">
    <div class="row">
    <div class="col-md-8">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="<?= $active['all'] ?>"><a href="/mind/"  aria-expanded="true"><?= isset($active['tag'])?'<span class="label label-info tag">'.$active['tag'].'</span>':'全部' ?></a></li>
                <li class="<?= $active['team'] ?>"><a href="/mind/team-index"  aria-expanded="false">组队&招募</a></li>
                <li class="<?= $active['question'] ?>"><a href="/mind/question-index" aria-expanded="false">问答</a></li>
                <li class="<?= $active['mind'] ?>"><a href="#settings" data-toggle="tab" aria-expanded="false">分享</a></li>
                <li class="<?= $active['match'] ?>"><a href="" data-toggle="tab" aria-expanded="false">活动</a></li>
            </ul>
            <div class="tab-content mind-tab">
                <div class="tab-pane active" id="activity">
                    <?php foreach ( $dataProvider->getModels() as $list):?>
                        <div class="post clearfix">
                            <div class="mind-slide">
                                <a class="avatar" href="<?= Url::to('user/'.$list->user_id)?>">
                                    <img src="<?= $users->find()->where(['id'=>$list->user_id])->one()->profile->gravatar_email  ?>">
                                </a>
                                <div class="count"><?= $list['PostCount']?></div>
                            </div>

                            <div class="mind-body">
                                <div class="row" style="width:100%;">
                                    <div class="post-block" >
                                        <a href="/mind/post?id=<?= $list['id']?>" class="post-title" target="_blank"><?=$list['title']?></a>
                                        <div><?= $list['user']['username']?></div>
                                    </div>
                                    <?php
                                    if($list['type'] == \common\models\Mind::TYPE_TEAM){
                                    if(!empty($list['img'])){   ?>
                                        <div class="col-sm-8">
                                            <div class="mind-content">
                                                <span class="mind-extra">  比赛：<?= $list['team']['race']?></span>
                                                <span class="mind-extra">  面向：<?= $list['team']['orienting']?></span>
                                                <div class="mind-content">
                                                    <?=  mb_substr($list['content'], 0,100, 'utf-8')?>
                                                </div>
                                            </div>
                                            <ul class="list-inline">
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                                                </li>
                                                <li class="pull-right">
                                                    <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                                                        (5)</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4" style="height: 8em;overflow: hidden;margin-bottom: 1em">
                                            <img class="img-responsive " src="<?=$list['img']?>" alt="Photo">
                                        </div>

                                    <?php }else{ ?>
                                        <div class="col-lg-12">
                                            <div class="mind-extra-wrapper">
                                                <div class="row row-post">
                                                    <div class="col-lg-5">
                                                        <dl class="dl-horizontal">

                                                            <dt>项目 :</dt> <dd><a class="text-navy"><?= $list['team']['race']?></a></dd>
                                                            <dt>人数:</dt> <dd><?= $list['team']['already_member']?>/<?= $list['team']['limit_member']?></dd>

                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-7" id="cluster_info">
                                                        <dl class="dl-horizontal">
                                                            <dt>面向:</dt> <dd><?= $list['team']['orienting']?></dd>
                                                            <dt>截止时间:</dt>
                                                            <dd class="project-people">
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                           <!-- <span class="mind-extra">  比赛：</span>
                                            <span class="mind-extra">  面向：</span>-->
                                            </div>
                                            <div class="mind-content">
                                                <?=  mb_substr($list['content'], 0,85, 'utf-8')?>
                                            </div>
                                            <ul class="list-inline">
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i>20赞</a></li>

                                                <li>
                                                    <a href="#" class="link-black text-sm">
                                                        <?= $list['PostCount']?>回复</a>
                                                </li>
                                                <li class="pull-right">
                                                    <a href="#" class="link-black text-sm">
                                                        <?= Yii::$app->formatter->asRelativeTime($list['create_at'],'now')?>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    <?php }
                                    }elseif($list['type'] == \common\models\Mind::TYPE_DOCUMENT){ ?>
                                        <div class="col-lg-12">
                                            <div class="mind-extra-wrapper">

                                            </div>
                                            <div class="mind-content">
                                                <?=  mb_substr($list['content'], 0,162, 'utf-8')?>
                                            </div>
                                            <ul class="list-inline">
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i>20赞</a></li>

                                                <li>
                                                    <a href="#" class="link-black text-sm">
                                                        <?= $list['PostCount']?>评论</a>
                                                </li>
                                                <li class="pull-right">
                                                    <a href="#" class="link-black text-sm">
                                                        <?= Yii::$app->formatter->asRelativeTime($list['create_at'],'now')?>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    <?php }else{

                                    if(!empty($list['img'])){ ?>
                                        <div class="col-sm-4" style="height: 8em;overflow: hidden;margin-bottom: 1em">

                                            <img class="img-responsive " src="<?=$list['img']?>" alt="Photo">

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="mind-content">
                                                <?= mb_substr($list['content'],0,85, 'utf-8')?>                                            </div>
                                            <ul class="list-inline">
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                                                <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                                                </li>
                                                <li class="pull-right">
                                                    <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                                                        (5)</a></li>
                                            </ul>
                                        </div>
                                    <?php }else{ ?>
                                    <div class="col-sm-12">
                                        <div class="mind-content">
                                            <?= mb_substr($list['content'],0,105, 'utf-8')?>
                                        </div>
                                        <ul class="list-inline panel-tools-hover">
                                            <li><a href="#" class="link-black text-sm"><i class="fa  fa-sort-up margin-r-5"></i>20赞</a></li>

                                            <li>
                                                <span href="#" class="link-black text-sm">
                                                <?= $list['PostCount']?>评论</span>

                                            </li>
                                            <li class="pull-right">
                                                <a href="#" class="link-black text-sm">

                                                    <?= Yii::$app->formatter->asRelativeTime($list['create_at'],'now')?>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                    <?php } }?>
                                </div>
                            </div>

                        </div>

                    <?php endforeach; ?>


                </div>


                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <div class="col-md-4">
            <div class="box  box-solid" style="box-shadow: none;background: none">
                <div class="box-body no-padding">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding: 5px;border: 1px solid #dbe3eb;">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                        </ol>
                        <div class="carousel-inner slider-items">
                            <!--<div class="item">
                                <img src="/adminlte/dist/img/library.jpg" alt="First slide">
                                <div class="carousel-caption">
                                    灰雀教务系统
                                </div>

                            </div>-->
                            <div class="item">
                                <img src="/images/newspic.jpg" alt="Second slide">

                                <div class="carousel-caption">
                                    Second Slide
                                </div>
                            </div>
                            <div class="item active">
                                <img src="/adminlte/dist/img/computer.jpg" alt="Third slide">

                                <div class="carousel-caption">
                                    Third Slide
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="fa fa-angle-right"></span>
                        </a>
                    </div>
                </div>
            </div>

        <div class="box box-solid">
            <!--<div class="box-header with-border">
                <h3 class="box-title">Labels</h3>

                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>-->
            <!-- /.box-header -->
            <div class="box-body no-padding">

                <ul class="nav nav-pills nav-stacked">
                    <li><a href="/mind/question-create?cate=1&type=1"><i class="fa fa-circle-o text-red"></i> 提问</a></li>
                    <li><a href="/mind/team-create?cate=1&type=2"><i class="fa fa-circle-o text-yellow"></i> 组队</a></li>
                    <li><a href="/mind/mind-create?cate=1&type=3"><i class="fa fa-circle-o text-light-blue"></i> 分享</a></li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">我的</h3>

                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body ">
                <ul class="nav nav-stacked">
                    <li><a href="#"><i class="fa fa-book margin-r-5"></i>
                            关 注 <span class=" badge bg-blue">31</span>
                        </a></li>
                    <li><a href="#"><i class="fa fa-book margin-r-5"></i>
                            邀 请 <span class=" badge bg-aqua">5</span>
                        </a></li>
                    <li><a href="#"><i class="fa fa-book margin-r-5"></i>
                            回 复 <span class=" badge bg-green">12</span>
                        </a></li>
                    <li><a href="#"><i class="fa fa-book margin-r-5"></i>
                            收 藏
                        </a></li>
                </ul>

            </div>
            <!-- /.box-body -->
        </div>

        <!-- /.box -->

            <img class="img-responsive pad" src="/adminlte/dist/img/photo2.png" alt="Photo">




    </div>
</div>
</div>



<?php


    //$this->registerJs($js);
    $this->registerJs("
    $('.create-mind').on('click', function () {
$('#select-mind').modal('show');
})

",\yii\web\View::POS_READY);


Modal::begin([
    'id' => 'select-mind',
    'header' => '<h4 class="modal-title">cc</h4>',
    //'footer' =>  Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']),
]);?>


<?php
Modal::end();
?>
