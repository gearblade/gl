<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MindSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '帖子管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mind-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('发布专题', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user.username',
            'title',

            //'username',

            'create_at',
            // 'img:ntext',
             'type',
            // 'likes',
            // 'oppose',
            // 'icon:ntext',
            // 'cate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
