<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 16-9-26
 * Time: 上午1:09
 */
?>
<h3 class="question-title"><?= $model->title ?></h3>
<div class="user-block">
    <img class="img-circle" src="/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">发起者：<?= $model['user']['username']?></a></span>
    <!-- <span class="description">描述：</span>-->
</div>
<div class="team-content">
    <div class="row">
        <div class="col-lg-5">
            <dl class="dl-horizontal">
                <dt>项目 :</dt> <dd><a class="text-navy"><?= $model['team']['race']?></a></dd>
                <dt>队伍人数:</dt> <dd>  <?= $model['team']['limit_member']?> </dd>
                <dt>招募人数:</dt> <dd><a href="#" class="text-navy"><?= $model['team']['already_member']?></a> </dd>
            </dl>
        </div>
        <div class="col-lg-7" id="cluster_info">
            <dl class="dl-horizontal">

                <dt>面向:</dt> <dd><?= $model['team']['orienting']?></dd>

                <dt>截止时间:</dt>
                <dd class="project-people">
                </dd>
            </dl>
        </div>
    </div>
    <?= $model->content ?>
</div>