<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Mind;
/* @var $this yii\web\View */
/* @var $model common\models\Mind */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Minds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/adminlte/plugins/mind/commit.js',['depends'=>'backend\assets\AdminLteAsset']);
?>
<div class="mind-view">

    <div class="row">
        <div class="col-md-8">
            <div class="box box-widget">
                <div class="box-header with-border post-question">
                    <?php
                    switch($model->type) {
                        case $model::TYPE_TEAM:
                            echo $this->render('_team_post', [
                                'model' => $model,
                            ]);
                            break;
                        case Mind::TYPE_QUESTION:
                            echo $this->render('_question_post', [
                                'model' => $model,
                            ]);
                            break;
                        default:
                            echo $this->render('_mind_post', [
                                'model' => $model,

                            ]);
                    }
                    ?>


                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="tab-pane active" id="activity">
                        <!-- Post -->
                        <?php foreach ( $dataProvider->getModels() as $list):?>
                        <div class="post clearfix">
                            <div class="mind-slide">
                                <a class="avatar"><img src="/adminlte/dist/img/avatar3.png"></a>
                                <div class="count">0</div>

                            </div>
                           <!-- <div class="avatar">
                                <a><img class="img-circle img-bordered-xs" src="/adminlte/dist/img/avatar3.png"></a>
                            </div>-->

                            <div class="mind-body">
                                <div class="row" style="width:100%;">
                                    <div class="post-block" >
                                        <a href="/mind/post?id=<?= $list['id']?>" class="post-username"><?=$list['user']['username']?></a>
                                        <div class="post-bio" style="">，<?=$list['user']['profile']['bio']?></div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="post-content">
                                            <?= htmlspecialchars_decode($list['content'])?>
                                        </div>
                                        <ul class="list-inline">
                                            <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i>20赞</a></li>
                                            <li><a href="#" class="link-black text-sm">反对</a>
                                            </li>
                                            <li>
                                                <span href="#" class="link-black text-sm commit">
                                                    <i class="fa fa-comments-o margin-r-5"></i><?= $list['CommentCount']?>评论</span>
                                            </li>
                                            <li class="pull-right">
                                                <a href="#" class="link-black text-sm">
                                                    <?= Yii::$app->formatter->format($list['create_at'],['datetime','php: Y/m/d '])?>
                                                </a>
                                            </li>

                                        </ul>
                                        <div class=" box-comments" data-open="0" data-id="<?=$list['id']?>" data-mind="<?=$model->id ?>">

                                        </div>
                                        <!--<input class="form-control input-sm" type="text" placeholder="Type a comment">-->

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.post -->
                        <?php endforeach; ?>
                       

                        <!-- /.post -->
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer -->
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">回答&回复
                        <!--<small>Simple and fast</small>-->
                    </h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                   <?= Yii::$app->view->render('//post/_form.php',['model'=>$postAnswer,'model_id'=>$model->id]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
