<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 16-9-26
 * Time: 上午1:09
 */
?>
<h3 class="question-title"><?= $model->title ?></h3>
<div class="tags">
<?php foreach ($model->tags as $tag) { ?>
    <span class="label label-info tag"><?= $tag->tag ?></span>
<?php } ?>
</div>
<div class="user-block">
    <img class="img-circle" src="/adminlte/dist/img/user1-128x128.jpg" alt="User Image">
    <span class="username"><a href="#">提问者：<?= $model['user']['username']?></a></span>
    <!-- <span class="description">描述：</span>-->
</div>
<div class="question-content">

    <?= $model->content ?>
</div>