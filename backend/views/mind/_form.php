<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\ueditor\UEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Mind */
/* @var $form yii\widgets\ActiveForm */
$template = '{label}<div class="col-sm-10">{input}{error}{hint}</div>';
$label = ['class' => "col-sm-2 text-c control-label form-label"];
?>
<div class="report-form  row">

    <div class="col-lg-8 ">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="text-center"> 分享自己的文章 </h4>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
            <div class="box-body">
                <?= $form->field($model, 'title', ['template' => $template])->textInput(['maxlength' => true,])
                    ->label('标题', ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($model, 'content', ['template' => $template])
                    ->widget(UEditor::className(),
                        ['class'=>'col-sm-10 ','id'=>'content','name'=>'content',
                            'clientOptions'=>[
                                'toolbars'=>[[
                                    'fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                                ]]]
                        ])
                    ->label('正文', ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($model, 'tag', ['template' => $template])->widget('common\widgets\tags\Tags')->label(null, ['class' => 'col-sm-2 control-label']) ?>

                <?= $form->field($model, 'img',['template' => $template])
                    ->widget('common\widgets\file_upload\FileUpload',[
                    'config'=>[]
                ])->label('封面', ['class' => 'col-sm-2 control-label']) ?>

                <div class="form-group">
                </div>
            </div>
            <div class="box-footer">

                <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

