<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Urls */

?>
<img src="/logo_r2.png" style="width: 100%;">
<div class="cate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
