<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Cate */

// $this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Cates', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="cate-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'weight',
            'price',
            'area',
            'destination',
            'user_name',
            'tel',
            'status',
            'current_node',
            'states',
            'create_time',
        ],
    ]) ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'path_id',
            'path_order',
            'area',
            'destination',
            'distance',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>