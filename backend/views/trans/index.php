<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Cate */

// $this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Cates', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="cate-view">

    <h1><?= Html::encode($this->title) ?></h1>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'area',
            'destination',
            'distance',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>