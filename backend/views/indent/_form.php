<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Indent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="indent-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'ext')->textarea(['maxlength' => true,]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '下单' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?=  Html::a('设置新地址', '#', [
            'id' => 'update_address',
            'data-toggle' => 'modal',
            'data-target' => '#update-modal',
            'class' => 'btn btn-info btn-xs',
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
