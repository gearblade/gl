<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Student', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'password',
            'name',
            'sex',
            // 'signature',
            // 'intro',
            // 'skill:ntext',
            // 'birthday',
            // 'college',
            // 'major',
            // 'class',
            // 'native',
            // 'politics',
            // 'address:ntext',
            // 'person_id',
            // 'tel',
            // 'email_t:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
