<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */
$this->registerMetaTag(['charset'=>'gb2312']);
?>
<?php
$js = <<<JS
    $('#response').click(function () {
        $.ajax({
            'type': 'GET',
            'url': 'http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG',
            'dataType': 'image/jpeg',
            'success': function(data,status,xhr){
                $('#response').text(xhr.getAllResponseHeaders());
                alert(xhr.getAllResponseHeaders());
            },
            
        })
    })

JS;
$this->registerJs($js);
//$pic = new stdClass();

?>
<div class="student-form row">
    <div class="col-md-offset-1 col-md-5">
        <div class="box box-widget">
    <div class="box-header with-border">
        <div class="user-block">
            <img class="" src="/images/icon/校徽.png" alt="User Image">
            <span class="username"><a href="#"> 教务账号绑定 </a></span>

        </div>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                <i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>

    </div>

    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']])->label('学号') ?>

        <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', '教务密码') ) ?>



        <?= $form->field($model, 'username')->textInput(['maxlength' => 100])->label('昵称') ?>

        <?= $form->field($model, 'ranstring',[
            'options'=>['class'=>'form-group col-xs-6','style'=>'padding-left: 0px;']
        ])->textInput(['placeholder'=>'验证码'])->label(false) ?>

        <span id="RandomPhoto col-xs-6"><img src=<?=$pic['pic']?> border=0 height=30 width=98 style='border: 1px solid #666' id='imgRandom'>
        <a href="javascript:GetPhoto()">看不清楚</a>
    </span>




        <?= $form->field($model, 'picId')->hiddenInput(['value' => $pic['cookie'] ])->label(false) ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? '绑定' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success  btn-block btn-flat' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
    </div>
</div>

<script>
    function GetPhoto()
    {
        var PhotoText=document.getElementById("RandomPhoto");
        PhotoText.innerHTML="<img src=http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG border=0 height=30 width=98 style='border: 1px solid #666' id='imgRandom'>&nbsp;<a href=\"javascript:GetPhotoAgain()\"><u>(不清楚)</u></a>";
    }

    function GetPhotoAgain()
    {
        document.getElementById("imgRandom").src="http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG?test="+new Date().getTime();
    }

</script>
