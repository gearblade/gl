<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 17-3-22
 * Time: 上午5:27
 */
?>
<div class="box box-solid">
    <div class="box-header ">
        <h1 class="box-title">搜索交大</h1>
    </div>
    <div class="box-body" style="padding-bottom: 20px;">
        <!--<input class="form-control input-lg" type="text" placeholder=".input-lg">
        <span class="input-group-btn">
            <button type="button" class="btn btn-info btn-flat">Go!</button>
        </span>-->
        <div class="input-group input-group-lg">
            <input type="text" class="form-control input-lg">
            <span class="input-group-btn">
                <button type="button" class="btn btn-info btn-flat"> 搜 索 ～ </button>
            </span>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<div class="row" style="    margin-bottom: 7px;">
    <div class="col-md-5" style="">
        <!-- About Me Box -->
        <div class="box box-widget index-box blue-border">
            <a href="/index.php/document/list?cate=32">
                <div class="box-header with-border index-box-header" style="">
                    <i class="index-box-icon bicon-laba"></i>
                    <h3 class="box-title index-box-title">热门资讯 </h3>

                </div></a>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=75">
                            西南交通大学第十届大学生交通科技大赛复赛答辩通知
                        </a>
                        <span class="text-muted pull-right">3/21</span>

                    </li>
                    <li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=74">
                            第八届中国大学生服务外包创新创业大赛校内报名通知
                        </a>
                        <span class="text-muted pull-right">3/17</span>

                    </li><li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=74">
                            西南交通大学2016-2017学年第二学期创新讲座安排汇总
                        </a>
                        <span class="text-muted pull-right">3/17</span>

                    </li><li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=74">
                            2017年西南交通大学数学建模论文写作比赛通知
                        </a>
                        <span class="text-muted pull-right">3/17</span>

                    </li>
                </ul>

            </div>

            <!-- /.box-body -->
        </div>

    </div>
    <div class="col-md-5" style="">
        <!-- About Me Box -->
        <div class="box box-widget index-box blue-border">
            <a href="/index.php/document/list?cate=32">
                <div class="box-header with-border index-box-header" style="">
                    <i class="index-box-icon bicon-laba"></i>
                    <h3 class="box-title index-box-title">热门资源 </h3>

                </div></a>

            <div class="box-body">
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=75">
                            教室借用申请书
                        </a>


                    </li>
                    <li class="list-group-item">
                        <a class="box-item " href=" /index.php/document/view?id=74">
                            毕业学位、学籍证明英文模板
                        </a>

                    </li>
                </ul>

            </div>

        </div>

    </div>

    <div class="col-md-2">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">常用链接</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body no-padding">

                <ul class="nav nav-pills nav-stacked">
                    <li><a href="http://portal.swjtu.edu.cn" target="_blank">校园信息门户</a>
                    <li><a href="http://ids.swjtu.edu.cn/amserver/UI/Login?goto=http://oa.swjtu.edu.cn/oasys/index.nsf/index?readform" target="_blank">OA办公平台</a>
                    <li><a href="http://mail.swjtu.edu.cn/" target="_blank">电子邮件</a>
                    <li><a href="http://www.swjtu.edu.cn/html/tsqk/1.html" target="_blank">图书期刊</a>
                    <li><a href="http://gs.swjtu.edu.cn/ac/home/index" target="_blank">学术委员会</a>
                    <li><a href="http://dean.swjtu.edu.cn/" target="_blank">教务信息</a>
                    <li><a href="http://homepage.swjtu.edu.cn" target="_blank">教师主页</a>
                    <li> <a href="http://inc.swjtu.edu.cn" target="_blank">网络服务</a>
                    <li> <a href="http://card.swjtu.edu.cn" target="_blank">一卡通</a> <a href="http://xb.swjtu.edu.cn/vis/" target="_blank">VI视觉识别系统</a>
                    <li> <a href="http://rscqa.swjtu.edu.cn/" target="_blank">人事问答</a>
                    <li><a href="http://bbs.swjtu.edu.cn" target="_blank">BBS</a>
                    <li><a href="http://114.swjtu.edu.cn/" target="_blank">交大黄页</a>
                    <li><a href="http://aqsc.swjtu.edu.cn" target="_blank">安全生产</a>
                    <li><a href="http://xb.swjtu.edu.cn/oa/" target="_blank">移动校园</a>
                    <li><a href="http://jw.swjtu.edu.cn/LeaderMailAdd-226.shtml" target="_blank">监督举报平台</a>
                    <li><a href="http://bidding.swjtu.edu.cn" target="_blank">招投标信息</a>
                    <li><a href="http://freshman.swjtu.edu.cn/" target="_blank">迎新网</a>
                </ul>
            </div>
        </div>
    </div>

</div>