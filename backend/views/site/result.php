<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 17-3-22
 * Time: 上午5:27
 */
?>
<div class="box box-solid">
    <div class="box-header ">
        <h1 class="box-title">搜索交大</h1>
    </div>
    <div class="box-body" style="padding-bottom: 20px;">
        <!--<input class="form-control input-lg" type="text" placeholder=".input-lg">
        <span class="input-group-btn">
            <button type="button" class="btn btn-info btn-flat">Go!</button>
        </span>-->
        <div class="input-group input-group-lg">
            <input type="text" class="form-control input-lg">
            <span class="input-group-btn">
                <button type="button" class="btn btn-info btn-flat"> 搜 索 ～ </button>
            </span>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<div class="row" style="    margin-bottom: 7px;">

    <div class="col-md-8 wbox">
        <div class="box box-widget index-box ">
            <a href="/index.php/document/list?cate=14" class="uppercase">
                <div class="box-header with-border index-box-header">
                    <i class="index-box-icon bicon-news"></i>

                    <h3 class="box-title index-box-title text-white">搜索结果 5条 </h3>
                    <div class="list-news-bg"></div>
                    <div class="box-tools pull-right">

                        <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>-->
                        <!--<button type="button" class="btn btn-box-tool"  title="" >
                            <i class="fa fa-comments"></i>更多</button>-->
                    </div>
                </div>
            </a>
            <div class="box-body box-profile blue-border">
                <div class="attachment-block ">
                    <div class="attachment-pushed">
                        <h4 class="attachment-heading listpic-text" style="font-size: 15px; height: 33px;">
                            <a href="/index.php/document/view?id=90">第八届中国大学生服务外包创新创业大赛校内报名通知</a>
                            <button  class="btn btn-box-tool" style="float: right" >2017-03-17</button>
                        </h4>
                        <div class="attachment-text listpic-text" style="height: 40px;">
                            “第八届中国大学生服务外包创新创业大赛”报名已经开始，我校将选拔若干只支参赛团队参加全国比赛（其中标准类A题拟报送6支团队，创新创业实践类B题拟报送1支团队）。               </div>
                    </div>
                </div>
                <div class="attachment-block ">
                    <div class="attachment-pushed">
                        <h4 class="attachment-heading listpic-text" style="font-size: 15px; height: 33px;">
                            <a href="/index.php/document/view?id=90">
                                我校学子在第七届“中国大学生服务外包创新创业大赛”中再创佳绩</a>
                            <button  class="btn btn-box-tool" style="float: right" >2016-09-26</button>
                        </h4>
                        <div class="attachment-text listpic-text" style="height: 40px;">
                            9月21日至23日，教育部、商务部、无锡市政府共同举办的第七届中国大学生服务外包创新创业大赛在江苏无锡举行。由我校尹帮旭、杨柳等老师指导、团委王蔚副书记、教务处李静波老师领队的13支队伍经过初赛评比、决赛答辩获得全国一等奖1项、二等奖9项、三等奖3项，获国家奖总数位居全国第一。                        </div>
                    </div>
                </div>
                <div class="attachment-block ">
                    <div class="attachment-pushed">
                        <h4 class="attachment-heading listpic-text" style="font-size: 15px; height: 33px;">
                            <a href="/index.php/document/view?id=90">第七届中国大学生服务外包创新创业大赛校内报名通知</a>
                            <button  class="btn btn-box-tool" style="float: right" >2016-03-09</button>
                        </h4>
                        <div class="attachment-text listpic-text" style="height: 40px;">
                            “第七届中国大学生服务外包创新创业大赛”报名已经开始，我校将选拔若干只支参赛团队参加全国比赛（其中标准类、创业实践类题目不限团队，自主类题目中文组、英文组各一组）。                     </div>
                </div>
                </div>
                <div class="attachment-block ">
                    <div class="attachment-pushed">
                        <h4 class="attachment-heading listpic-text" style="font-size: 15px; height: 33px;">
                            <a href="/index.php/document/view?id=90">第六届中国大学生服务外包创新创业大赛（全国赛）报名通知</a>
                            <button  class="btn btn-box-tool" style="float: right" >2015-03-30</button>
                        </h4>
                        <div class="attachment-text listpic-text" style="height: 40px;">
                            “第六届中国大学生服务外包创新创业大赛”报名已经开始，我校将安排六支参赛团队参加全国比赛（标准类题目、自主类题目及创业实践类各两队，共六队）。                     </div>
                </div>
                </div>
                <div class="attachment-block ">
                    <div class="attachment-pushed">
                        <h4 class="attachment-heading listpic-text" style="font-size: 15px; height: 33px;">
                            <a href="/index.php/document/view?id=90">我校学子在第四届大学生服务外包创新创业大赛上再创佳绩
                            </a>
                            <button  class="btn btn-box-tool" style="float: right" >2013-08-29</button>
                        </h4>
                        <div class="attachment-text listpic-text" style="height: 40px;">
                            8月20日至21日，第四届中国大学生服务外包创新创业大赛在无锡市成功举办，我校代表队获得标准类企业命题组全国总决赛一等奖，连续两年取得一等奖的优异成绩。                    </div>
                </div>
                </div>

            </div>

        </div>

    </div>

    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">相关搜索</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!--<strong><i class="fa fa-book margin-r-5"></i> Education</strong>-->

                <p class="text-muted">
                    服务外包
                </p>
                <p class="text-muted">
                    服务外包大赛 保研
                </p>
                <p class="text-muted">
                    服务外包大赛 获奖
                </p><p class="text-muted">
                    教务之星工作室
                </p><p class="text-muted">
                    节能减排大赛
                </p>


            </div>
        </div>
    </div>

</div>