<?php

namespace backend\controllers;

use backend\models\Tag;
use common\models\Post;
use common\models\PostSearch;
use common\models\Team;
use common\models\Transpath2;
use common\models\Transdeal;
use common\models\User;
use common\models\Transpathnode;
use Yii;
use common\models\TranspathSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransController implements the CRUD actions for Tracs model.
 */
class TransController extends Controller
{
    public $layout = 'main_nav';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transdeal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Transdeal::find()->where(['user_id'=>Yii::$app->user->id]),
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
     /**
     * Create a single Transdeal model.
     * @param integer $area
     * @return mixed
     */
    public function actionCreate()
    {   
        $model = new Transdeal();

        if ($model->load(Yii::$app->request->post())) {
            $model = $this->calculDeal($model);
            if(!$model->save())  throw new NotFoundHttpException('The requested deal save failed.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

     /**
     * Search a single Transdeal model.
     * @param integer $area
     * @return mixed
     */
    public function actionSearch()
    {   
        $model = new Transdeal();

        if ($model->load(Yii::$app->request->post())) {
            $model = $this->calculDeal($model);
            $dataProvider = new ActiveDataProvider([
                'query' => Transpathnode::find()->where(['path_id'=>$model->path_id]),
            ]);
            return $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('_search', [
                'model' => $model,
            ]);
        }
    }
  /**
     * Updates an existing Transdeal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Transdeal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

        
    /**
     * Displays a single Transdeal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Transpathnode::find()->where(['path_id'=>$model->path_id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }
   
    protected function findModel($id)
    {
        if (($model = Transdeal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }   

    protected function calculDeal($model)
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        $model->user_id = $user->id;
        $model->user_name = $user->username;
        $model->tel = $user->tel;

        $path = Transpath2::find()->where(['area' => $model->area,'destination' => $model->destination])->one();

        if(!$path)  throw new NotFoundHttpException('The requested path does not exist.');

        $model->path_id = $path->id;
        $model->area_id = $path->area_id;
        $model->destination_id = $path->destination_id;
        $model->price = $model->weight * $path->price;

        $model->status = 0;
        $model->current_node = 0;

        return $model;
    }
}



















  // $model = new Transdeal();
        // var_dump($model);die();
        // $model->path_id = $path->path_id;
        // $model->area_id = $path->area_id;
        // $model->destination_id = $path->destination_id;
        // $model->price = $model->weight * $path->price;


        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     // $model->validate();
        //     // $searchModel = new TranspathSearch();
        //     // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //     // var_dump($model);die();
        //     // $path = Transpath2::find()->where(['area' => $model->area,'destination' => $model->destination])->one();
        //     var_dump($model);die();

        //     return $this->redirect(['create',
        //         // 'searchModel' => $searchModel,
        //         // 'dataProvider' => $dataProvider, 
        //         'path' => $path,
        //     ]);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }



        // return $this->render('list', [
        //     // 'model' => $model,
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);



 /*$result = "";
        $path = Transpath2::find()->where(['area_id' => $area_id,'destination_id' => $destination_id])->one();
        // $path = Transpath2::find()->where(['area' => $area,'destination' => $destination])->one();
        //http://www.gl.com/fpath/list?area_id=15&destination_id=8

        $result = $result . "{\"Transpath\":{\"id\":\"".$path->id."\",\"area\":\"".$path->area."\",\"area_id\":\"".$path->area_id."\",\"destination\":\"".$path->destination."\",\"destination_id\":\"".$path->destination_id."\",\"distance\":\"".$path->distance."\",\"price\":\"".$path->price."\",\"rate\":\"".$path->rate."\",\"office\":\"".$path->office."\",\"office_id\":\"".$path->office_id."\"}}";

        $pathnode = Transpathnode::find()->where(['path_id' => $path->id])->all();
        
        foreach ($pathnode as $val) {
            $result = $result . "{\"Transpath\":{\"id\":\"".$val->id."\",\"path_id\":\"".$val->path_id."\",\"path_order\":\"".$val->path_order."\",\"area_id\":\"".$val->area_id."\",\"destination_id\":\"".$val->destination_id."\",\"distance\":\"".$val->distance."\",\"price\":\"".$val->price."\",\"rate\":\"".$val->rate."\"}}";
        }

        return $result;*/


          // $searchModel = new TranspathSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $model = new Transpath2();

        // $model->validate();
            // $searchModel = new TranspathSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // var_dump($model);die();
            // $path = Transpath2::find()->where(['area' => $model->area,'destination' => $model->destination])->one();
             // var_dump($model);die();

        //Transpathnode::find()->where(['path_id' => $path_id])->all();

        // $count = $this->dataProvider->getCount();
        // var_dump($dataProvider);die();