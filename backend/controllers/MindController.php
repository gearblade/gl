<?php

namespace backend\controllers;

use backend\models\Tag;
use common\models\Post;
use common\models\PostSearch;
use common\models\Team;
use Yii;
use common\models\Mind;
use common\models\search\MindSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MindController implements the CRUD actions for Mind model.
 */
class MindController extends Controller
{
    public $layout = 'main_nav';
    public $active = ['all'=>null,'match'=>null,'team'=>null,'question'=>null,'mind'=>null];
    public $type = '';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mind models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MindSearch();
        /*MindSearch[tag]=js
        MindSearch%5Btag%5D=js*/
        #$dataProvider = $searchModel->searchTag(Yii::$app->request->queryParams);
        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);

        if( !empty(Yii::$app->request->get('tag')) ){
            /*$tag = new Tag();
            $tag->tag = Yii::$app->request->get('tag');*/
            $tags = Tag::findAll(['tag'=>Yii::$app->request->get('tag')]);
            $this->active['tag'] = Yii::$app->request->get('tag');
//var_dump($tags);die();
            $dataProvider = $searchModel->searchTag(Yii::$app->request->queryParams,$tags);

        }
        $this->active['all'] = 'active';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active' => $this->active,
        ]);
    }
    public function actionAdmin()
    {
        $this->layout='/main';
        $searchModel = new MindSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->active['all'] = 'active';
        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active' => $this->active,
        ]);
    }
    public function actionTeamIndex()
    {
        $searchModel = new MindSearch();
        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams,$searchModel::TYPE_TEAM);
            //->andFilterWhere(['type'=>$searchModel::TYPE_TEAM]);

        $this->active['team'] = 'active';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active' => $this->active,
        ]);
    }
    public function actionQuestionIndex()
    {
        $searchModel = new MindSearch();
        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams,$searchModel::TYPE_QUESTION);
        //->andFilterWhere(['type'=>$searchModel::TYPE_TEAM]);

        $this->active['question'] = 'active';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active' => $this->active,
        ]);
    }
    public function actionMind()
    {
        $searchModel = new MindSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('mind', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mind model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPost($id)
    {
        $postsSearch = new PostSearch();
        $dataProvider = $postsSearch->search(['PostSearch'=>['mind_id'=>$id,'post_id'=>0]]);

        $postAnswer = new Post();

        $postCommit = new Post(['scenario' => 'commit']);
        $model =$this->findModel($id);
        switch($model->type){
            case $model::TYPE_TEAM:
                return $this->render('post', [
                    'model' => $model,
                    'postsSearch' => $postsSearch,
                    'dataProvider'=>$dataProvider,
                    'postAnswer'=>$postAnswer,
                    'postCommit'=>$postCommit,
                ]);
                break;
            case Mind::TYPE_QUESTION:
                return $this->render('post', [
                    'model' => $model,
                    'postsSearch' => $postsSearch,
                    'dataProvider'=>$dataProvider,
                    'postAnswer'=>$postAnswer,
                    'postCommit'=>$postCommit,
                ]);
                break;
            default:
                return $this->render('post', [
                'model' => $model,
                'postsSearch' => $postsSearch,
                'dataProvider'=>$dataProvider,
                'postAnswer'=>$postAnswer,
                'postCommit'=>$postCommit,
            ]);


        }

    }

    /**
     * Creates a new Mind model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionTeamCreate($cate)
    {
        $model = new Mind();
        $team =  new Team();
        $model->user_id = Yii::$app->user->id;
        $team->mind_id= 0;
        if ($model->load(Yii::$app->request->post())
            && $model->validate()
            &&$team->load(Yii::$app->request->post())

        ) {
            $model->type = $model::TYPE_TEAM;
            /*try{
    $team->save();
    $model->save();
}catch (Exception $e){

}*/
            if($model->save()){
                $team->mind_id = $model->id;
                $model->setTag(Yii::$app->request->post('Mind')['tag']);
                if($team->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }else{
                    $model->delete();
                }
            }
            return $this->render('create', [
                'model' => $model,
                'team' => $team,
                'type' => 'team',
            ]);


        } else {
            return $this->render('create', [
                'model' => $model,
                'team' => $team,
                'type' => 'team',
            ]);
        }
    }

    public function actionMindCreate($type,$cate)
    {
        $model = new Mind();
        $model->user_id = Yii::$app->user->id;
        $model->type = $model::TYPE_MIND;
        $model->tag = Yii::$app->request->post('Mind')['tag'];
        #echo $model->tag;die();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->setTag(Yii::$app->request->post('Mind')['tag']);
            #$model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'type' => 'mind',
            ]);
        }
    }
    public function actionQuestionCreate($type,$cate)
    {
        $model = new Mind();
        $model->user_id = Yii::$app->user->id;
        $model->type = $model::TYPE_QUESTION;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setTag(Yii::$app->request->post('Mind')['tag']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'type' => 'question',
            ]);
        }
    }

    /**
     * Updates an existing Mind model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Mind model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Mind model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mind the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mind::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
