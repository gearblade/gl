<?php

namespace backend\controllers;

use backend\models\Package;
use backend\models\Tag;
use Yii;
use backend\models\Book;
use backend\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'excel' => ['POST'],
                ],
            ],
        ];
    }
    public $enableCsrfValidation = false;
    public function actions()
    {
        return [
            'upload'=>[
                'class' => 'common\widgets\file_upload\UploadAction',     //这里扩展地址别写错
                'config' => [
                    'imagePathFormat' => "/images/upload/{yyyy}{mm}{dd}/{time}{rand:6}",
                ]
            ]
        ];
    }
    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main_nav';
        //$pages = new Pagination([  'pageSize' => 18]);

        $searchModel = new BookSearch();
       /* $totalCount = $searchModel->search(Yii::$app->request->queryParams)
            ->getCount();*/
        /*->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();*/

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);

        if( !empty(Yii::$app->request->get('tag')) ){
            /*$tag = new Tag();
            $tag->tag = Yii::$app->request->get('tag');*/
            $tags = Tag::findAll(['tag'=>Yii::$app->request->get('tag')]);
//var_dump($tags);die();
            $dataProvider = $searchModel->searchTag(Yii::$app->request->queryParams,$tags);
        }
       /* $count = $dataProvider->getCount();
        $totalCount = $dataProvider->getTotalCount();
        $dataProvider->*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }
    public function actionSearch()
    {
        $this->layout = 'main_nav';
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAdmin()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExcel(){
        //$this->enableCsrfValidation = false;
        $book = new Book();
        $msg ='';
        if(Yii::$app->request->isPost){
            //echo 'sss';die();
            $msg = $book->excelInto();
        }

        return $this->render('excel', ['msg'=>$msg]);
    }
    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'main_nav';
        $package_status = 0;
        $package =  Package::findOne(['book_id'=>$id,'user_id'=>Yii::$app->user->id]);
        //var_dump($package);die();
        if($package!==null){
            $package_status = $package->status;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'package_status'=>$package_status,
        ]);
    }

    public function actionDetail($id)
    {
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionCollect($book){
        $package = new Package();
        $package->status  = Package::STATUS_COLLECT;
        $package->book_id = $book;
        $package->user_id = Yii::$app->user->id;
        $package->save();


    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
