<?php

namespace backend\controllers;

use backend\models\Book;
use backend\models\Package;
use dektrium\user\models\Profile;
use dektrium\user\models\SettingsForm;
use Yii;
use backend\models\Indent;
use backend\models\IndentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IndentController implements the CRUD actions for Indent model.
 */
class IndentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Indent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Indent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Indent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($book_id)
    {
        $this->layout = 'main_nav';

        $model = new Indent();
        $model->book_id = $book_id;
        $model->user_id = Yii::$app->user->id;
        $model->school = Yii::$app->user->identity->profile->school;
        $model->house = Yii::$app->user->identity->profile->house;
        $model->room = Yii::$app->user->identity->profile->room;
        $model->tel = Yii::$app->user->identity->tel;
        $model->address = Yii::$app->user->identity->profile->school_part.'校区_'
        .Yii::$app->user->identity->profile->house.'_'.Yii::$app->user->identity->profile->room.'号';

        $model->status = $model::UnderDeal;
        $model->money = 0;
        $model->costomer = Yii::$app->user->identity->profile->name;

        $address = Profile::findOne(['user_id'=>Yii::$app->user->id]);


        $book = Book::findOne($book_id);

        if(!(($model->school && $model->house && $model->room&& $model->tel|| $model->house)&&$model->costomer) ){

            //return $this->redirect(['/user/settings/profile-modal', 'modal'=>1]);
            return $this->render('create', [
                'model' => $model,
                'book' => $book,
                'address' => $address,
                'modal' => ['title'=>'请先设置收货信息']
            ]);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $package = new Package();
            $package->book_id = $model->id;
            $package->user_id = $model->user_id;
            $package->status = $model->status;
            $package->save();
            //echo $package->book_id;
            //echo $package->user_id;
            //die();

            return $this->redirect(['/package/index', ]);

        } else {
            return $this->render('create', [
                'model' => $model,
                'book' => $book,
                'address' => $address,
            ]);
        }
    }
    public function actionSuccess(){
        $this->layout = "main_nav";

        $model = new Indent();

        return $this->render('success', [

            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Indent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Indent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Indent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
