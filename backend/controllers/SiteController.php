<?php
namespace backend\controllers;

use common\models\search\RaceSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use yii\mongodb\Collection;
/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'main_nav.php';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','index'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex()
    {
        //$name ="";
        return $this->redirect('/mind/');
        //return $this->render('index',[]);
    }

    /**
     * 搜索主页
     */
    public function actionSearch(){
        return $this->render('search',[]);
    }
    public function actionResult(){
        return $this->render('result',[]);
    }
    public function actionStartup()
    {
        //$name ="";

        return $this->render('index',[]);
    }
    public function actionRace()
    {
        //$name ="";
        $dataSearch = new RaceSearch();
        $dataProvider =$dataSearch->search([]);


        return $this->render('race',[
            'postsSearch' => $dataSearch,
            'dataProvider'=>$dataProvider,]);
    }

    public function actionLogin()
    {
        $this->layout = 'login.php';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
            //echo "hah";
            //return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
