<?php

namespace backend\controllers;
use backend\models\UserRegister;
use dektrium\user\models\LoginForm;
use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationTel;
use dektrium\user\models\Token;
use dektrium\user\models\User;
use dektrium\user\helpers\Password;
use dektrium\user\traits\AjaxValidationTrait;
use dektrium\user\traits\EventTrait;

class RegisterController extends \yii\web\Controller
{
    use AjaxValidationTrait;
    use EventTrait;

    /**
     * Event is triggered after creating RegistrationForm class.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_BEFORE_REGISTER = 'beforeRegister';

    /**
     * Event is triggered after successful registration.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_AFTER_REGISTER = 'afterRegister';

    public $layout ='main_nav';
    public function actionIndex()
    {
        $model = new UserRegister();

        $model->scenario = 'register';

        $this->performAjaxValidation($model);

        if(\Yii::$app->request->isPost){
            $model->load(\Yii::$app->request->post());
            $model->password_hash =  Password::hash( $model->password );
            $model->confirmed_at =  time();
            $model->auth_key =  \Yii::$app->security->generateRandomString();

            if ( $model->save()) {

                $profile = new Profile();
                $profile->user_id = $model->id;
                $profile->save();
                $token = new Token();
                $token->user_id = $model->id;
                $token->type = Token::TYPE_CONFIRMATION;
                $token->save();

                $auth = \Yii::$app->authManager;
                $role = $auth->createRole('学生');
                $auth->assign($role, $model->id);
                //$this->trigger(self::EVENT_AFTER_REGISTER, $event);
                $login = \Yii::createObject(LoginForm::className());
                //$login = new LoginForm();
                $login->login = $model->username;
                $login->password = $model->password;
                $login->login();

                return $this->redirect(['/user/settings/profile','modal'=>1]);

               /* return $this->render('model', [
                    'title'  => \Yii::t('user', 'Your account has been created'),
                    //'module' => $this->module,
                ]);*/
            }
        }


        return $this->render('registertel', [
            'model'  => $model,
            //'module' => $this->module,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = '//main_nav';
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $model = new UserRegister();

        $model->scenario = 'login';
       // $model->\Yii::$app->getRequest()->post());

        $model->attributes = \Yii::$app->request->post('UserRegister');
        //var_dump($model);

        //var_dump($model->$loginStr);die();
        if(\Yii::$app->request->isPost){
            $model->loginStr = \Yii::$app->request->post('UserRegister')['loginStr'];
            if (  $model->login()) {

                return $this->goBack();
            }
        }


        return $this->render('login', [
            'model'  => $model,

        ]);
    }

    public function actionTel(){
        $tel = \Yii::$app->request->post('tel');
        $num = rand(7393,34562);
        \Yii::$app->redis->set('tel'.\Yii::$app->user->id,$num);
        \Yii::$app->redis->expire('tel'.\Yii::$app->user->id,60*10);
        //Yii::$app->redis->set
        /*$session = Yii::$app->session;
        $session->set('language', 'en-US');*/
        //Yii::$app->smser->sendByTemplate($tel, ['100006'], 1);
        //var_dump(Yii::$app->smser->send('18190012014', '您好，您的验证码是1253【宿宿】'));
//echo $tel;

        $api = "http://api.sms.cn/sms/?ac=send&uid=quadrant&pwd=115a9eb20491c6ee78d22bd69d9cd528&template=100006&mobile=$tel&content={\"code\":\"$num\"}";
        //echo file_get_contents($api);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api);
        //curl_setopt($ch, CURLOPT_GET, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result,true); //返回发送状态
        //var_dump($result);die();
        if($result['stat'] == 100){
            $return = ['status'=>1,'msg'=>'短信验证码已发送'];
            echo (json_encode($return));
        }else{
            echo json_encode(['status'=>1,'msg'=>'暂停服务']);
        }
        //$return = ['status'=>1,'msg'=>'验证短信已发送'];

    }

}
