<?php

namespace backend\controllers;

use backend\models\UserRegister;
use dektrium\user\models\RegistrationTel;
use Yii;
use common\models\Student;
use common\models\search\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBind(){
        $student = new Student();

        $student->ranstring= Yii::$app->request->post('Student')['ranstring'];
        $student->username= Yii::$app->request->post('Student')['username'];
        //var_dump(Yii::$app->request->post('Student'));die();
        if($student->load(Yii::$app->request->post()) && $student->bind()){

            $user = new UserRegister();  //注册，用户表写入

            if($user->register(Yii::$app->request->post('Student'))){

                $this->redirect('/user/profile');

            }else{
                var_dump($user->getErrors());
                echo 'cc';die();
            }
        }
        $pic = $student->getRanstringPic();
        return $this->render('bind',['model'=>$student,'str'=>'cc','pic'=>$pic]);
    }

    public function actionLogin(){
        $student = new Student();

        $student->ranstring= Yii::$app->request->post('Student')['ranstring'];

        if($student->load(Yii::$app->request->post()) && $student->bind()){
            $user = new RegistrationTel(['user_id'=>$student->user_id,'password'=>$student->password ]);
            $this->redirect('/user/profile');
        }

        $pic = $student->getRanstringPic();
        return $this->render('bind',['model'=>$student,'str'=>'cc','pic'=>$pic]);
    }

    public function actionTest(){
        $student = new Student();

        if(Yii::$app->request->isPost){
            $cookies=Yii::$app->request->cookies;//('JSESSIONID');die();
            //echo $cookies->get('JSESSIONID');die();

            $info['user_id'] = '20132195'; //$map['count'];
            $info['password'] = 'phibeta';//$map['password'];
            $info['user_type'] = 'student';
            $info['set_language'] = 'cn';
            $info['ranstring'] = $_POST['Student']['ranstring'];
            $cookie_file = $_POST['Student']['picId'];
            //echo $_POST['Student']['picId'];die();

            //$cookie_file = $info['picId'];//tempnam('./temp22','uu');   //创建临时文件保存cookie
            $login_url = 'http://jiaowu.swjtu.edu.cn/servlet/UserLoginSQLAction';//登陆地址
//$post_fields = '__VIEWSTATE=dDwtMTk3MjM2MzU0MDs7Po+Vuw2g98nkvMhqN2OzPbC6DnbA&TextBox1='.$username&TextBox2='.$password;//POST参数
            $ch = curl_init($login_url); //初始化
            curl_setopt($ch, CURLOPT_HEADER, 1); //0显示
            curl_setopt($ch, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //1不显示
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookie_file);   //保存cookie
            curl_setopt($ch, CURLOPT_COOKIEFILE,  $cookie_file);
            curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookie_file);   //保存cookie

            curl_setopt($ch, CURLOPT_POST, 1);//POST数据
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($info));//加上POST变量
            $re = curl_exec($ch);
            curl_close($ch);

            $url2='http://jiaowu.swjtu.edu.cn/servlet/StudentInfoMapAction?MapID=101&PageUrl=../student/student/student.jsp';
            $ch2 = curl_init($url2);
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch2, CURLOPT_COOKIE, $cookie_file);
            curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie_file);
            curl_setopt($ch2, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");

            curl_setopt($ch2, CURLOPT_AUTOREFERER,1);
            curl_setopt($ch2, CURLOPT_HEADER, 0);
            curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch2, CURLOPT_REFERER, 'http://jiaowu.swjtu.edu.cn/');
            $contents = curl_exec($ch2);  //执行并获取HTML文档内容
//iconv('GBK', 'UTF-8', $contents);//编码转换
            curl_close($ch2);
            //echo $contents;
            unlink($cookie_file);
            $preg = '/ffff\".*>&nbsp;.*<\/td>/';
            preg_match_all($preg,$contents,$infoList);
            //$info = [];
            function listInfo($c){
                $info = preg_split('/(>&nbsp;)|(<\/td>)/',$c);
                return $info[1];
            }
            foreach($infoList[0] as $k=>$v){
                //echo "<br>".$k;
                //$a2 = preg_split('/(>&nbsp;)|(<\/td>)/',$v);
                switch($k){
                    case 0:$info['user_id'] = listInfo($v);break;
                    case 1:$info['name'] = listInfo($v);break;
                    case 3:$info['sex'] = listInfo($v);break;
                    case 4:$info['birthday'] = listInfo($v);break;
                    case 5:$info['college'] = listInfo($v);break;
                    case 7:$info['major'] = listInfo($v);break;
                    case 8:$info['class'] = listInfo($v);break;
                    case 9:$info['native'] = listInfo($v);break;
                    case 10:$info['nation'] = listInfo($v);break;
                    case 11:$info['politics'] = listInfo($v);break;
                    case 12:$info['person_id'] = listInfo($v);break;
                    case 15:$info['tel'] = listInfo($v);break;
                    case 16:$info['address'] = listInfo($v);break;
                    default:break;
                }
            }

            if(empty($info['name'])){
                var_dump($info);die();
                return  $re;;
            }else{
                /*if(!empty($map['code'])){
                    $wechat = new Wechat();  //微信对象
                    $wechatInfo = $wechat->getUserInfo($map['code']);   //获取用户微信信息
                    $info['openid'] = $wechatInfo['openid'];
                    $info['headimgurl'] = $wechatInfo['headimgurl'];
                }*/

                //$id = $this->add($info);
                //$info['uid'] = $id;
                //$info['power'] = $this->power;
                var_dump($info);die();
                return $info;   //返回给UindexModel
            }

            return $re;
            //return $this->render('bind',['model'=>$student,'str'=>$re]); BA4584663DE869D7D3682B35BC0F4E48
        }
        $url = "http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG".'?test='.time();//1490088525544;

        $path = '.' . \Yii::getAlias('@web') . '/images/ranstring/';  //保存路径

        $filename = $path . 'curl.jpeg';  // 'testqr.png';
        $pic = $this->getPic($url, $filename);

        return $this->render('bind',['model'=>$student,'str'=>'cc','pic'=>$pic]);
    } //

    protected function getPic($url = "", $filename = "")
    {
        if (is_dir(basename($filename))) {
            echo "The Dir was not exits";
            Return false;
        }
        //去除URL连接上面可能的引号
        $url = preg_replace('/(?:^[\'"]+|[\'"\/]+$)/', '', $url);
        //$path = '.' . \Yii::getAlias('@web') . '/images/ranstring/';
        $cookie_file = tempnam('temp22','uu');

        //var_dump($cookie_file);die();
        $first = curl_init();
        curl_setopt($first, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");

        curl_setopt($first, CURLOPT_URL, 'http://jiaowu.swjtu.edu.cn/service/login.jsp');
        curl_setopt($first, CURLOPT_HEADER, 1);
        //curl_setopt($first, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($first,CURLOPT_RETURNTRANSFER,true);

        curl_setopt($first, CURLOPT_COOKIEJAR,  $cookie_file);
        curl_setopt($first, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($first, CURLOPT_COOKIEJAR,  $cookie_file);

        curl_setopt($first, CURLOPT_TIMEOUT, 60);
        curl_exec($first);
        //curl_getinfo($hander);
        curl_close($first);
        exec('chmod 777 '.$cookie_file);

        $hander = curl_init();  //获取图片
        //$fp = fopen($filename, 'wb');
        curl_setopt($hander, CURLOPT_URL, $url);
        //curl_setopt($hander, CURLOPT_FILE, $fp);
        //curl_setopt($hander, CURLOPT_HEADER, 1);
        curl_setopt($hander, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");

        curl_setopt($hander, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($hander, CURLINFO_HEADER_OUT, TRUE); //请求头
        curl_setopt($hander,CURLOPT_RETURNTRANSFER,true);//以数据流的方式返回数据,当为false是直接显示出来

        //curl_setopt($hander, CURLOPT_COOKIEJAR,  $cookie_file);//保存cookie
        curl_setopt($hander, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($hander, CURLOPT_COOKIEJAR,  $cookie_file);//保存cookie

        curl_setopt($hander, CURLOPT_TIMEOUT, 60);
        /*$options = array(
        CURLOPT_URL=> 'http://jb51.net/content/uploadfile/201106/thum-f3ccdd27d2000e3f9255a7e3e2c4880020110622095243.jpg',
        CURLOPT_FILE => $fp,
        CURLOPT_HEADER => 0,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_TIMEOUT => 60
        );
        curl_setopt_array($hander, $options);*/

        $output = curl_exec($hander);
        //curl_getinfo($hander);
        curl_close($hander);

        file_put_contents($filename,$output);
        //fclose($fp);
        //exec('chmod 777 '.$cookie_file);
        exec('chmod 777 '.$filename);
        Return ['cookie'=>$cookie_file,'pic'=>$filename];

    }
    public function actionGetPic()
    {
        $url = "http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG";


        $path = '.' . \Yii::getAlias('@web') . '/images/ranstring/';  //保存路径

        $filename = $path . 'curl.jpeg'; //'testqr.png';

        /*
        *@通过curl方式获取制定的图片到本地
        *@ 完整的图片地址
        *@ 要存储的文件名
        */
        function getImg($url = "", $filename = "")
        {
            if (is_dir(basename($filename))) {
                echo "The Dir was not exits";
                Return false;
            }
            //去除URL连接上面可能的引号
            $url = preg_replace('/(?:^[\'"]+|[\'"\/]+$)/', '', $url);
            //$path = '.' . \Yii::getAlias('@web') . '/images/ranstring/';
            $cookie_file = tempnam('temp22','uu');

            echo $cookie_file;
            //var_dump($cookie_file);die();
            $hander = curl_init();

            //$fp = fopen($filename, 'wb');
            curl_setopt($hander, CURLOPT_URL, $url);
            //curl_setopt($hander, CURLOPT_FILE, $fp);
            curl_setopt($hander, CURLOPT_HEADER, 0);

            curl_setopt($hander, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($hander, CURLINFO_HEADER_OUT, TRUE); //请求头
//curl_setopt($hander,CURLOPT_RETURNTRANSFER,false);//以数据流的方式返回数据,当为false是直接显示出来

            curl_setopt($hander, CURLOPT_COOKIEFILE,  $cookie_file);
            curl_setopt($hander, CURLOPT_COOKIEJAR,  $cookie_file);//保存cookie

            curl_setopt($hander, CURLOPT_TIMEOUT, 60);
            /*$options = array(
            CURLOPT_URL=> 'http://jb51.net/content/uploadfile/201106/thum-f3ccdd27d2000e3f9255a7e3e2c4880020110622095243.jpg',
            CURLOPT_FILE => $fp,
            CURLOPT_HEADER => 0,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT => 60
            );
            curl_setopt_array($hander, $options);
            */

            $a=curl_exec($hander);
            //curl_getinfo($hander);
            curl_close($hander);
            //fclose($fp);
            var_dump($a);
            Return true;

        }
        return getImg($url, $filename);
    }
    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
