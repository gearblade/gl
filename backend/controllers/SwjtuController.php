<?php

namespace backend\controllers;

use backend\models\Tag;
use common\models\Post;
use common\models\PostSearch;
use common\models\Team;
use Yii;
use common\models\Link;
use common\models\Searchis;
use common\models\Information;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SwjtuController implements the CRUD actions for Mind model.
 */
class SwjtuController extends Controller
{
    public $layout = 'null';
    public $type = '';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return 
     */
    public function actionIndex()
    {
        $dataInformationProvider = new ActiveDataProvider([
            'query' => Information::find()->where(['level'=>0]),
        ]);
        $dataLinkProvider = new ActiveDataProvider([
            'query' => Link::find()->where([]),
        ]);
        $dataSearchisProvider = new ActiveDataProvider([
            'query' => Searchis::find()->where([]),
        ]);
        return $this->render('index', [
            'dataInformationProvider' => $dataInformationProvider,
            'dataLinkProvider' => $dataLinkProvider,
            'dataSearchisProvider' => $dataSearchisProvider,
        ]);
    }

    /**
     * @return 
     */
    public function actionSearch($search)
    {
        $model = Searchis::find()->where(['title' => $search])->one(); 
        if(!$model) {
            $model = new Searchis();
            $model->title = $search;
            $model->save();
        };

        $uri = "http://cn.bing.com/search?q=" . $search;
        $this->redirect($uri);
    }
}
