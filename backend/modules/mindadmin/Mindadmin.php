<?php

namespace backend\modules\Mindadmin;

/**
 * admin module definition class
 */
class Mindadmin extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\mindadmin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
