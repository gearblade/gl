<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\TeamSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mind_id') ?>

    <?= $form->field($model, 'uname') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'race') ?>

    <?php // echo $form->field($model, 'limit_member') ?>

    <?php // echo $form->field($model, 'already_member') ?>

    <?php // echo $form->field($model, 'connect') ?>

    <?php // echo $form->field($model, 'orienting') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
