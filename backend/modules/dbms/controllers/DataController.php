<?php

namespace backend\modules\dbms\controllers;

use backend\models\Data;
use backend\modules\dbms\models\UploadForm;
use yii\web\UploadedFile;
use Yii;
use yii\base\Object;
use yii\data\Pagination;
use backend\modules\dbms\models;
use backend\modules\dbms\models\DataFile;
use Codeception\Module\DataFactory;
use yii\mongodb\file\Query;
class DataController extends \yii\web\Controller
{
   
	/**
	 * 文件上传
	 * @return string
	 */
    public function actionUpload()
    {
    	//资料对应的课程
    	$course_id = 1;//Yii::$app->request->get('book_id');
    	$dataModel = new data();
    	$fileModel = new UploadForm();
    	if (Yii::$app->request->isPost) {
    		$fileModel->file = UploadedFile::getInstance($fileModel, 'file');
    		$content = $fileModel->upload();
    		if ($content) {
    			$post = Yii::$app->request->post();
    			$content['course_id'] = $course_id;
    			$res = $dataModel->saveData($content, $post);
    			if ($res)
    				Yii::$app->session->setFlash('success', '上传文件成功');//前端返回文件上传结果
    			else
    				Yii::$app->session->setFlash('danger', '上传文件失败');
    			
    		}
    		else {
    			Yii::$app->session->setFlash('danger', '上传文件失败'.$fileModel->hasErrors());
    		}
    	}
   /* 	$book = Book::find()  //获取对应书籍
    	->where(['id' => $book_id])
    	->select(['name', 'edition'])
    	->asArray()
    	->one();
    	*/
    	return $this->render('upload',
    			[
    					'fileModel'=> $fileModel,
    					'dataModel' => $dataModel,
    			//		'book' => $book,
    	
    			]);
    }
    
    /**
     * 管理mongodb里的数据库
     * @return string
     */
    public function actionManage()
    {
		$curPage = Yii::$app->request->get('page', 1);	//获得当前资料页码
		$model = new Data();
		$data = $model->getData($curPage, 2);
		$page = new Pagination(['totalCount'=>$data['count'], 'pageSize' => $data['pageSize']]);
		$data['page'] = $page;
    	return $this->render('manage',['data' => $data]);
    }
    
    /**
     * 下载mongodb中的资料
     * @param unknown $files_id
     */
    public function actionDownload()
    {
    	$_id = Yii::$app->request->get('_id');	
    	$_id = $_id['oid'];
		$model = new Data();
		if($model->isExists($_id))
		{
			echo '成功';
			$title = $model->getTitle($_id);	//得到数据的标题
			if(!$model->download($_id, $title))			//如果下载失败
			{
				Yii::$app->session->setFlash('danger', '系统错误,文件下载失败');//前端返回文件下载结果
			}else 
			{
				$model->upCounter($_id);
				echo '真的成功了';
			}
		}else 
		{
			echo '失败';
			Yii::$app->session->setFlash('warning', '系统错误,文件不存在');//前端返回文件下载结果
		}
    	
    }
    
	/**
	 * 编辑文档的信息
	 */
    public function actionUpdate()
    {
    	$_id = Yii::$app->request->get('_id');
    	$_id = $_id['oid'];
  		if (Yii::$app->request->isPost)
    	{	
    		 $post = Yii::$app->request->post();
	    	 $model = Data::findOne($post['Data']['_id']);
	    	 if ($model->load(Yii::$app->request->post()) && $model->save()) 
	    	 {
	           	return $this->redirect(['data/view', '_id' =>$model->_id]);
	         }
    	} else {
    		$model = Data::findOne($_id);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    /**
     * 查看文档的详细信息
     */
    public function actionView()
    {
    	$_id = Yii::$app->request->get('_id');
    	$_id = $_id['oid'];
    	$model = new Data();
    	$data = $model->getSingleData($_id);
    	return $this->render('view', ['data' => $data]);
    }
    
    /**
     * 删除文件
     * @return \yii\web\Response
     */
    public function actionDelete()
    {	
    	$_id = Yii::$app->request->get('_id');
    	$_id = $_id['oid'];
    	$model = Data::findOne($_id);
    	$model->delete();	//删除文档信息
    	$result = Yii::$app->mongodb
			    	  ->getFileCollection()
			    	  ->delete($model['files_id']);	//删除文件
    	return $this->redirect(['data/manage']);
    }
	
  
}









