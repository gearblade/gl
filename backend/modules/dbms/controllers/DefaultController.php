<?php

namespace backend\modules\dbms\controllers;

use yii\web\Controller;
use Yii;
/**
 * Default controller for the `dbms` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
/*	public function actionIndex()
	{
		$collection = Yii::$app->mongodb->getCollection ( 'gl' );
		$res = $collection->insert ( [
				'name' => 'John Smith22',
				'status' => 2
		] );
		var_dump($res);
		
		return $this->render('index');
	}
	*/
}
