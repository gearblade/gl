<?php
/**
*@author:kinredon
*@author:2017年1月13日
*文档上传表单模型
*/
namespace backend\modules\dbms\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\mongodb\file\ActiveRecord;
use yii\mongodb\file\Upload;
use Yii;
class UploadForm extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $file;
	public $content;
	public function rules()
	{
		return [
				[['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, pdf, docx, ppt, doc, txt, rar, zip'],
				//[['summary'], 'string'],
				//[['title', 'author', 'location', 'uploadtime', 'score', 'type', 'size'], 'required'],

		];
	}

	public function upload()
	{
		$document  = Yii::$app->mongodb
							  ->getFileCollection()
							  ->createUpload()
							  ->addContent($this->file)
							  ->complete();
		if ($this->validate() && $document) 
		{
			$this->content['size'] = $this->file->size;
			$this->content['type'] = $this->file->extension;
			$this->content['title'] = $this->file->baseName;
			$this->content['files_id'] = $document['_id'];		
			return $this->content;	
		}
		else 
		{
			return false;
		}
	}
	public function attributeLabels()
	{
		return [
				'file' => '选择文件'
		];
	}
}
?>