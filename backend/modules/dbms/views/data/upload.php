<?php
/**
*文件上传页面前端模板
*/
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = "文件上传";
?>

<div class="container libs-upload">
	<div class="box box-solid">
		<div class="box-header with-border">
		</div>
		<div class="box-body">
			<?php $form = ActiveForm::begin([
					'options' => ['enctype' => 'multipart/form-data',
							'class' => 'form',
					]
			]) ?>
<div class="row">
	<div class="col-md-6" style="border-right: 1px solid #e5e5e5;margin-bottom: 15px">

		<?= $form->field($fileModel, 'file')->fileInput() ?>
		<?= $form->field($dataModel, 'publicity')->dropDownList([1=>'本校',2=>'公开'])?>
		<?= $form->field($dataModel, 'summary')->textarea(['rows'=>3])?>
		<?= Html::submitButton("开始上传", ['class' => 'btn btn-info btn-flat margin upload-btn'])?>

	</div>
	<div class="col-md-6">

		<div class="overlay" style="    position: relative;
    top: 84px;">
			<i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>
</div>
			<div class="row" >
				<div class="col-md-12">
				</div>
			</div>
			<?php ActiveForm::end() ?>
			<table class="table table-bordered">
				<tbody><!---->

				<tr>

					<td class="text-green">成功导入</td>
					<td class="text-green">0 本</td>
					<td>
						<div class="progress progress-xs">
							<div class="progress-bar progress-bar-yellow" style="width: 1%"></div>
						</div>
					</td>
					<td><span class="badge bg-yellow">1%</span></td>
				</tr>
				<tr>
					<td class="text-red">失败</td>
					<td class="text-red">0本</td>

				</tr>
				<tr>
					<th>反馈信息</th>
					<th></th>

				</tr>

				</tbody></table>
		</div>
	</div>
</div>