<?php

namespace backend\modules\transpath\controllers;

use yii\web\Controller;
use common\models\Transpath2;
use common\models\Cites;
use common\models\Transpathnode;
use backend\modules\transpath\controllers\DIJkstra;

//include "path.class.php";

/**
 * Default controller for the `transpath` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

     /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCalcul()
    {
    	$cites_count = Cites::find()->count();
    	$trans_path = Transpath2::find()->all();

    	$v = [];
       
        for ($i=1; $i <= $cites_count; $i++) { 
            for ($j=1; $j <= $cites_count; $j++) { 
                $v[$i][$j] = 99999999;
            }
        }

    	foreach($trans_path as $list){
    		$v[$list->area_id][$list->destination_id] = $list->distance;
            $v[$list->destination_id][$list->area_id] = $list->distance;
    	}
        
    	$dijkstra = new DIJkstra($cites_count,$v);

        // $area_id = 1;
        // $destination_id = 15;

        // $res = $dijkstra->path($area_id,$destination_id);

        // echo "最短路径为:".$res[0]."<br>";
        // foreach ($res[1] as $key => $value) {
        //     echo "$value  ";
        // }



    	for ($i=1; $i < $cites_count; $i++) { 
    		for ($j=1; $j < $cites_count; $j++) { 

                if($i==$j) continue;

                //Data
                $area_id = $i;
                $destination_id = $j;

                //Search Path
    			$res = $dijkstra->path($area_id,$destination_id);

                $pathmodel = Transpath2::find()->where(['area_id' => $area_id,'destination_id' => $destination_id])->one(); 
                if(!$pathmodel) {
                    $pathmodel = new Transpath2();
                    $pathmodel->price = -5;
                };

                //Transpath2 Save
                $pathmodel->area = Cites::find()->where(['id' => $area_id])->one()->cityname; 
                $pathmodel->destination = Cites::find()->where(['id' => $destination_id])->one()->cityname;
                $pathmodel->area_id = $area_id;
                $pathmodel->destination_id = $destination_id;
                $pathmodel->save();

                $path_id = Transpath2::find()->where(['area_id' => $area_id,'destination_id' => $destination_id])->one()->id; 
                $pre_value = -1;
                $path_order = 0;
                $path_distance = 0;
                $path_price = 0;

				foreach ($res[1] as $key => $value) {

                    if ($pre_value == -1) {
                        $pre_value = $value;
                        continue;
                    }
                    $pathmodel2 = Transpath2::find()->where(['area_id' => $pre_value,'destination_id' => $value])->one();
                    if(!$pathmodel2) {
                        $pathmodel2 = Transpath2::find()->where(['area_id' => $value,'destination_id' => $pre_value])->one();

                        $path_distance = $path_distance + $pathmodel2->distance;
                        $path_price = $path_price + $pathmodel2->price;
                        $pre_value = $value;

                        //Transpathnode Save
                        $nodemodel = new Transpathnode();
                        $nodemodel->path_id = $path_id;
                        $nodemodel->path_order = $path_order++;
                        $nodemodel->area_id = $pathmodel2->destination_id;
                        $nodemodel->area = $pathmodel2->destination;
                        $nodemodel->destination_id = $pathmodel2->area_id;
                        $nodemodel->destination = $pathmodel2->area;
                        $nodemodel->distance = $pathmodel2->distance;
                        $nodemodel->price = $pathmodel2->price;
                        $nodemodel->save();
                    }else{
                        $path_distance = $path_distance + $pathmodel2->distance;
                        $path_price = $path_price + $pathmodel2->price;
                        $pre_value = $value;

                        //Transpathnode Save
                        $nodemodel = new Transpathnode();
                        $nodemodel->path_id = $path_id;
                        $nodemodel->path_order = $path_order++;
                        $nodemodel->area_id = $pathmodel2->area_id;
                        $nodemodel->area = $pathmodel2->area;
                        $nodemodel->destination_id = $pathmodel2->destination_id;
                        $nodemodel->destination = $pathmodel2->destination;
                        $nodemodel->distance = $pathmodel2->distance;
                        $nodemodel->price = $pathmodel2->price;
                        $nodemodel->save();
                    }
				}
                //Transpath2 Save
                $pathmodel->distance = $path_distance;
                $pathmodel->price = $path_price;
                $pathmodel->office_id = $pathmodel2->office_id;;
                if(!$path_distance || !$path_price) {
                    $pathmodel->delete();
                }else{ 
                    $pathmodel->save();
                }
    		}
    	}
        return $this->render('index');
    }
}
