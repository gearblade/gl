<?php
namespace backend\modules\transpath\controllers;
class DIJkstra{
    /*
     * 静态属性：顶点数，权值
     */
    private $n;
    private $w;//二维数组
    /*
     *构造函数，获取顶点数，边数，权值
     */
    public function __construct($n,$w)
    {
        $this->w[] = array(); 
        $this->n = $n;
        for($i=0;$i<$n;$i++)
        {
            for($j=0;$j<$n;$j++)
                if(isset($w[$i][$j]))
                    $this->w[$i][$j]=$w[$i][$j];
                else
                    $this->w[$i][$j]=99999999;
        }
    //    var_dump($this->w);
    }
    /*
     * 求取最短路径！
     */
    public function path($start,$end)
    {
        //定义辅助数组dist，S,以及存储路径的二维数组path
        $S = array();
        $dist = array();
        $path = array_fill(0,$this->n,array_fill(0,$this->n,array()));

        for($i=0;$i<$this->n;$i++)
        {
            $dist[$i]=$this->w[$start][$i];
            if($dist[$i]!=-1)
            {
                //在数组path[i]末尾添加结点
                array_push($path[$i], $start);
                array_push($path[$i], $i);
            }
        }
        for($m=0;$m<$this->n-1;$m++)
        {
            for($j=0;$j<$this->n;$j++)
                if($j!=$start&&!in_array($j,$S))
                    break;
            for($i=$j+1;$i<$this->n;$i++)
                if($i!=$start&&$dist[$i]<$dist[$j]&&!in_array($i,$S))
                    $j=$i;
            array_push($S, $j);
            for($i=0;$i<$this->n;$i++)
                if($i!=$start&&!in_array($i, $S))
                {
                    $d=$dist[$j]+$this->w[$j][$i];
                    if($dist[$i]>$d)
                    {
                        $dist[$i]=$d;
                        $path[$i]=$path[$j];
                        array_push($path[$i], $i);
                    }
                }
        }
        // for($i=0;$i<$this->n;$i++)
        //     unset($path[$end][$i]);
        $rel_path = array();
        for($i=$this->n,$j=0;$i<count($path[$end]);$i++,$j++)
            $rel_path[$j] = $path[$end][$i];
        $res = array(
            '0'=>$dist[$end],
            '1'=>$rel_path,
        );
        return $res;//返回结果的第一个元素为最短路径，第二个元素为具体路径
    }
};  

?>