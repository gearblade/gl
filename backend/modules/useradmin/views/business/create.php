<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Room */

$this->title = '新增房屋';
$this->params['breadcrumbs'][] = ['label' => '房东', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li><a href="#tab_3-1" data-toggle="tab">Tab 1</a></li>
        <li><a  disabled data-toggle="tab">Tab 2</a></li>
        <li class="active"><a href="#tab_1-2" data-toggle="tab">第一步</a></li>
        <!--<li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Dropdown <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </li>-->
        <li class="pull-left header"><i class="fa fa-th"></i> 新房屋 </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1-2">
            <div class="room-create ">

                <?= $this->render('_form', [
                    'model' => $model,
                    'facility' => $facility,
                ]) ?>

            </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2-2">

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3-2">

        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>

