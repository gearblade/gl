<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\data\Pagination;
use backend\widgets\common\LinkPages;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '出租房管理台';
$this->params['breadcrumbs'][] = '房东';
$this->params['breadcrumbs'][] =  $this->title;
//$this->registerCssFile('@web/ex/js/advanced-datatable/css/demo_page.css',[]);
//$this->registerCssFile('@web/ex/js/advanced-datatable/css/demo_table.css',[]);
//$this->registerCssFile('@web/ex/js/data-tables/DT_bootstrap.css',[]);
//$this->registerCssFile('@web/media/css/DT_bootstrap.css',[ 'depends'=> 'frontend\assets\MetronicAsset']);
//$this->registerJsFile('@web/media/js/select2.min.js',['depends'=>['frontend\assets\MetronicAsset']]);
//$this->registerJsFile('@web/media/js/jquery.dataTables.min.js',['depends'=>['frontend\assets\MetronicAsset']]);
//$this->registerJsFile('@web/media/js/DT_bootstrap.js',['depends'=>['frontend\assets\MetronicAsset']]);
//$this->registerJsFile('@web/ex/js/advanced-datatable/js/jquery.dataTables.js',['depends'=>['backend\assets\AdminLteAsset']]);
//$this->registerJsFile('@web/ex/js/data-tables/DT_bootstrap.js',['depends'=>['backend\assets\AdminLteAsset']]);
//$this->registerJsFile('@web/ex/js/dynamic_table_init.js',['depends'=>['backend\assets\AdminLteAsset']]);


$data = $dataProvider->getModels();
$count = $dataProvider->getCount();
$pages = new Pagination(['totalCount' => $count, 'pageSize' => 18]);
?>
<div class="row">
<div class="col-md-12 ">

    <div class="box">
        <div class="box-body">
            <a class="btn btn-app blue-btn">
                <i class="fa fa-edit"></i> 长租
            </a>
            <a class="btn btn-app blue-btn">
                <i class="fa fa-edit"></i> 短租
            </a>

            <a class="btn btn-app blue-btn">
                <i class="fa fa-save"></i> 设置
            </a>
            <a class="btn btn-app blue-btn">
                <span class="badge bg-yellow">3</span>
                <i class="fa fa-bullhorn"></i> 日志
            </a>

            <a class="btn btn-app blue-btn">
                <span class="badge bg-purple">891</span>
                <i class="fa fa-users"></i> 房客
            </a>

            <a class="btn btn-app blue-btn">
                <span class="badge bg-aqua">12</span>
                <i class="fa fa-envelope"></i> 消息
            </a>
            <a class="btn btn-app blue-btn">

                <i class="fa fa-heart-o"></i> 帮助
            </a>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Start Quick Menu -->
    <!-- End Quick Menu -->

</div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="btn-group">
                    <a href='<?= Url::toRoute('business/create') ?>' id="btn editable-sample_new" class="btn btn-primary">
                        <i class="fa fa-plus"></i> 新 增
                    </a>
                </div>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 300px;top: 6px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>

            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead >
                    <tr role="row" style="font-size: 14px;">


                        <th>
                            <a href="<?= \yii\helpers\Url::current(['sort' => (Yii::$app->request->get('sort') == 'status') ? '-status' : 'status']) ?>">状态</a>
                        </th>


                        <th ><a href="<?= \yii\helpers\Url::current(['sort' => 'title']) ?>">标题</a></th>

                        <th><a
                                href="<?= \yii\helpers\Url::current(['sort' => (Yii::$app->request->get('sort') == 'price') ? '-price' : 'price']) ?>">价格</a>
                        </th>

                        <th ><a
                                href="<?= \yii\helpers\Url::current(['sort' => (Yii::$app->request->get('sort') == 'createtime') ? '-createtime' : 'createtime']) ?>">起始时间</a>
                        </th>
                        <th class=""></th>


                        <!--
                            <th class="sorting_asc" tabindex="0" aria-controls="example0" rowspan="1" colspan="1"
                                aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                style="width: 133px;">Name
                            </th>-->

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $list): ?>
                        <tr>

                            <td><?= ($list['status'] == 1) ? '出售中' : '已租' ?></td>

                            <td class="">
                                <a target="_blank" href="<?= Url::to('@web/room/view?id=' . $list['id']) ?>"><?= Html::encode($list['title']) ?></a>

                                <p>地址：<?= Html::encode($list['address']) ?></p></td>

                            <td ><?= Html::encode($list['price']) ?></td>

                            <td><?= Html:: encode((strtotime($list['createtime']) > 0) ? date('Y-m-d', strtotime($list['createtime'])) : '未设置') ?></td>


                            <td> <?= Html::a('修改', ['update', 'id' => $list['id']], ['class' => 'btn btn-default btn-xs']) ?>
                                <?= Html::a('删除', ['delete', 'id' => $list['id']], [
                                    'class' => 'btn btn-danger  btn-xs',
                                    'data' => [
                                        'confirm' => '确定要删除这条商品吗？',
                                        'method' => 'post',
                                    ],
                                ]) ?></td>

                            <!-- <td class="hidden-480" style="display: none">A</td>
                             <td class="hidden-480" style="display: none">A</td>
                             <td class="hidden-480" style="display: none">A</td>-->

                        </tr>
                    <?php endforeach; ?>

                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>





