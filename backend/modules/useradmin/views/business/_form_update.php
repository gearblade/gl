<?php
//namespace frontend\modules\useradmin\views\business ;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\captcha\Captcha;
use common\widgets\ueditor\UEditor;
use common\widgets\webuploader\MultiImage;
use yii\helpers\Url;
//use iisns\webuploader\MultiImage;
use shiyang\masonry\Masonry;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Room */
/* @var $form yii\widgets\ActiveForm */
$template = '{label}<div class="col-sm-10">{input}{error}{hint}</div>';
$label = ['class'=>"col-sm-2 text-c control-label form-label"];
//$this->registerCssFile('@web/adminlte/plugins/iCheck/all.css',[ 'depends'=> 'backend\assets\AdminLteAsset']);
//$this->registerCssFile('@web/adminlte/plugins/iCheck/minimal/_all.css',[ 'depends'=> 'backend\assets\AdminLteAsset']);
//$this->registerJsFile('@web/adminlte/plugins/iCheck/icheck.js',[ 'depends'=> 'backend\assets\AdminLteAsset']);



?>

<div class="room-form form-horizontal">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data'],
    ]); /*$form->options['id']='form-horizontal'; *//* var_dump($form->options);die();*/ ?>
    <?= $form->field($model, 'title',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control md-input"])->label(null,$label)  ?>

    <?= $form->field($model, 'price',['template' => $template])->input('number',['class'=>"form-control sm-input"])->label(null,$label) ?>


    <?= $form->field($model, 'connect',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control sm-input "])->label(null,$label) ?>

    <?= $form->field($model, 'atrribute',['template' => $template])->dropDownList(['单间/套一'=>'单间/套一','独栋'=>'独栋','套二'=>'套二','套三'=>'套三','套四'=>'套四','套五'=>'套五'],['class'=>"form-control select2-container span3"])->label(null,$label) ?>
    <?= $form->field($model, 'toilet',['template' => $template])->dropDownList(['独卫'=>'独卫','共卫'=>'共卫','二卫'=>'二卫'],['class'=>"form-control select2-container span3"])->label(null,$label) ?>
    <?= $form->field($model, 'kitchen',['template' => $template])->dropDownList(['有'=>'有','无'=>'无'],['class'=>"form-control select2-container span3"])->label(null,$label) ?>

    <?= $form->field($model, 'area',['template' => $template])->input('number',['class'=>"form-control sm-input"])->label(null,$label) ?>
    <?= $form->field($model, 'address',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control md-input "])->label(null,$label) ?>

    <?php echo $form->field($facility, 'facility',['template' => $template])->checkBoxList(
        ['空调'=>'空调','热水'=>'热水','网络'=>'网络','衣柜'=>'衣柜','电梯'=>'电梯','向阳'=>'向阳'],
        ['class'=>'  icheckbox_flat-green checked facility-lable',])
        ->label(null,['class'=>"col-sm-2 control-label form-label "]) ?>

    <?= $form->field($model, 'userlimit',['template' => $template])->input('number',['class'=>"form-control sm-input"])->label(null,$label) ?>
    <?= $form->field($model, 'pay',['template' => $template])->dropDownList(['面议'=>'面议','押一付一'=>'押一付一','押一付二'=>'押一付二','押一付三'=>'押一付三'],['class'=>"form-control  select2-container md-input"])->label(null,$label) ?>

    <?= $form->field($model, 'content',['template' => '{label}<div class="col-md-7 col-sm-12">{input}{error}{hint}</div>'])->label(null,$label)->widget(UEditor::className(),['class'=>'col-md-7 form-control','id'=>'content','name'=>'content', ])  ?>

    <?= $form->field($model, 'pic',['template' => $template])->widget('common\widgets\file_upload\FileUpload',[
        'config'=>[]
    ])->label(null, ['class' => 'col-sm-2 control-label']) ?>


    <div class="row">
        <div class=" col-md-offset-1  col-md-10  col-sm-12 ">
        <?php
        $p1 = $p2 = [];
        foreach ($model->photos['photos'] as  $k => $v) {
            $p1[$k] = $v['savepath'];
            $p2[$k] = ['url' => '/useradmin/business/picdelete', 'key' => $v['id']];
        }
        echo $form->field($model, 'ext')->label('房间相册')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                // 需要预览的文件格式
                'previewFileType' => 'image',
                // 预览的文件
                'initialPreview' => $p1,
                // 需要展示的图片设置，比如图片的宽度等
                'initialPreviewConfig' => $p2,
                // 是否展示预览图
                'initialPreviewAsData' => true,
                // 异步上传的接口地址设置
                'uploadUrl' => Url::toRoute(['/useradmin/business/create_pic?id='.$model->id]),
                // 异步上传需要携带的其他参数，比如商品id等
                'uploadExtraData' => [
                    'id' => $model->id,
                ],
                'uploadAsync' => true,
                // 最少上传的文件个数限制
                'minFileCount' => 1,
                // 最多上传的文件个数限制
                'maxFileCount' => 10,
                // 是否显示移除按钮，指input上面的移除按钮，非具体图片上的移除按钮
                'showRemove' => true,
                // 是否显示上传按钮，指input上面的上传按钮，非具体图片上的上传按钮
                'showUpload' => true,
                //是否显示[选择]按钮,指input上面的[选择]按钮,非具体图片上的上传按钮
                'showBrowse' => true,
                // 展示图片区域是否可点击选择多文件
                'browseOnZoneClick' => true,
                // 如果要设置具体图片上的移除、上传和展示按钮，需要设置该选项
                'fileActionSettings' => [
                    // 设置具体图片的查看属性为false,默认为true
                   // 'showZoom' => false,
                    // 设置具体图片的上传属性为true,默认为true
                    'showUpload' => true,
                    // 设置具体图片的移除属性为true,默认为true
                    'showRemove' => true,
                ],
            ],
            // 一些事件行为
            /*'pluginEvents' => [
                // 上传成功后的回调方法，需要的可查看data后再做具体操作，一般不需要设置
                "fileuploaded" => "function (event, data, id, index) {
                console.log(data);
            }",
            ],*/
        ]);
        ?>

        </div>
    </div>
<script>
    /*$(document).ready(function(){
        $(".room-form").click(function(){
            alert('haha');
        });
    });*/

  /*  $(document).ready(function(){

        $(".img-edit").click(function(event){
            alert('haha');
        });

    });*/
</script>



    <!--<div class="form-actions">

        <button type="submit" class="btn green">Validate</button>

        <button type="button" class="btn">Cancel</button>

    </div>-->
    <div class="form-actions">
        <?= Html::submitButton($model->isNewRecord ? '创建' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success green' : 'btn btn-primary green']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
