<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Race */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="race-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'leader')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'guider')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'introduce')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rule')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'expecteddate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'connect')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
