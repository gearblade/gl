<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Race */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="race-view">
    <div class="row">
    <div class="col-md-7">
        <div class="box box-widget">
           <!-- <div class="box-header with-border">

            </div>-->
            <!-- /.box-header -->
            <div class="box-body">
                <h3 class="font-bold m-b-xs">
                    <?= $model->name ?>
                </h3>
                <small>China Undergraduate Mathematical Contest in Modelling</small>

                <hr>

                <h5>大赛简介</h5>

                <div class="small text-muted">
                    <?= $model->introduce ?>

                </div>
                <dl class="small dl-horizontal">
                    <dt>竞赛时间</dt>
                    <dd><?= $model->expecteddate ?></dd>
                    <dt>领队老师</dt>
                    <dd><?= $model->leader ?></dd>
                    <dt>指导老师</dt>
                    <dd><?= $model->guider ?></dd>
                    <dt>联系方式</dt>
                    <dd><?= $model->connect ?></dd>
                    <dt>官方网址</dt>
                    <dd><?= $model->website ?></dd>
                </dl>
                <dl class="small m-t-md">

                    <dt>赛事规则</dt>
                    <dd><?= $model->rule ?></dd>

                </dl>
                <hr>

            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
        </div>


    </div>
        <div class="col-md-4" style="   ">
            <?= backend\widgets\box\BoxWidget::widget(['cate' => 10, 'title' => '相关新闻', 'liNum' => 5,
                'url' => Url::toRoute(['document/list', 'cate' => 10]),
                'css'=>['warper'=>'box-widget index-box','title'=>'with-border','body'=>'box-profile']]) ?>

            <!-- /.nav-tabs-custom -->
        </div>
    </div>



</div>
