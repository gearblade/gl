<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 16-5-19
 * Time: 上午6:45
 */

use yii\widgets\ActiveForm;
use common\widgets\file_upload\FileUpload;   //引入扩展
use yii\helpers\Html;
$template = '{label}<div class="col-sm-10">{input}{error}{hint}</div>';
$label = ['class'=>"col-lg-2 col-sm-2 control-label"];
//echo FileUpload::widget('common\widgets\file_upload\FileUpload',['config'=>['value'=>'ccc']]) ;
$this->registerJsFile('/adminlte/plugins/msg/verify.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<div class="row">
<div class="col-lg-10">
    <section class="panel">
        <header class="panel-heading">
            房东身份验证
        </header>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['options' => ['class'=>'form-horizontal']]); ?>
            <!--<form class="form-horizontal" role="form">-->

                <?= $form->field($model, 'name',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control md-input"])->label(null,$label) ?>


                <?= $form->field($model, 'person_id',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control "])->label(null,$label) ?>

                <br/>


            <?= $form->field($model, 'person_card',['template' => $template])->widget('common\widgets\file_upload\FileUpload',[
                'config'=>[]
            ])->label(null, ['class' => 'col-sm-2 control-label']) ?>

            <?= $form->field($model, 'infro',['template' => $template])->textarea(['rows' => 6,'class'=>"form-control "])->label(null,$label) ?>

            <?= $form->field($model, 'weibo',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control "])->label(null,$label) ?>
            <?= $form->field($model, 'tel',['template' => $template])->textInput(['maxlength' => true,'class'=>"form-control md-input"])->label(null,$label) ?>

            <?php $templateMsg =  '{label}<div class="col-sm-10 "> <div class="input-group  md-input">{input} <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" id="btn-virefy">获取验证短信</button>
                    </span> </div>
                    <p id="virefy-hint"></p>{error}{hint}</div>'; ?>
            <?= $form->field($model, 'virefyMsg',['template' => $templateMsg])->textInput(['maxlength' => true,'class'=>"form-control "])->label(null,$label) ?>


                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <?= Html::submitButton($model->isNewRecord ? '保存' : '保存', ['class' => $model->isNewRecord ? 'btn ' : 'btn ']) ?>
                    </div>
                </div>
                    <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>



</div>