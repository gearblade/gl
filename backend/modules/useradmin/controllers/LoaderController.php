<?php

namespace backend\modules\useradmin\controllers;

use common\models\User;
use Yii;
use common\models\Loader;
use common\models\LoaderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoaderController implements the CRUD actions for Loader model.
 */
class LoaderController extends Controller
{
    //public $layout='ex.php';
    public function actions()
    {
        return [
            'upload'=>[
                'class' => 'common\widgets\file_upload\UploadAction',     //这里扩展地址别写错
                'config' => [
                    'imagePathFormat' => "/images/upload/{yyyy}{mm}{dd}/{time}{rand:6}",
                ]
            ]
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
public function actionTest(){
    return $this->render('test');
}

    /**
     * Lists all Loader models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new LoaderSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = Loader::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
        if($model==null){
            $this->redirect(['create']);
        }
        return $this->render('index', [
            'model' => $model//$this->findModel(Yii::$app->user->identity->id),
        ]);
        /*return $this->render('index', [
            'model' => $this->findModel(Yii::$app->user->identity->id),
            //'dataProvider' => $dataProvider,
        ]);*/
    }

    /**
     * Displays a single Loader model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$this->findModel(Yii::$app->user->identity->id
        $model = Loader::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Loader model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Loader();
        $model->user_id = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $auth = Yii::$app->authManager;
            $role = $auth->createRole('房东');
            $auth->assign($role, $model->user_id);
            /*$user = User::findOne(Yii::$app->user->identity->id);
            $user->status = User::STATUS_LOADER;

            $user->save();*/

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Loader model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionAdmin()
    {
        $searchModel = new LoaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Deletes an existing Loader model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Loader model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loader the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTel(){
        $tel = Yii::$app->request->post('tel');
        $num = rand(7393,34562);
        Yii::$app->redis->set('tel'.Yii::$app->user->id,$num);
        Yii::$app->redis->expire('tel'.Yii::$app->user->id,60*10);
        //Yii::$app->redis->set
        /*$session = Yii::$app->session;
        $session->set('language', 'en-US');*/
        //Yii::$app->smser->sendByTemplate($tel, ['100006'], 1);
        //var_dump(Yii::$app->smser->send('18190012014', '您好，您的验证码是1253【宿宿】'));
//echo $tel;
        
        $api = "http://api.sms.cn/sms/?ac=send&uid=quadrant&pwd=115a9eb20491c6ee78d22bd69d9cd528&template=100006&mobile=$tel&content={\"code\":\"$num\"}";
        //echo file_get_contents($api);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api);
        //curl_setopt($ch, CURLOPT_GET, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result); //返回发送状态
        if($result['stat'] == 100){
            $return = ['status'=>1,'msg'=>'验证码已发送'];
            echo (json_encode($return));
        }
        //$return = ['status'=>1,'msg'=>'验证短信已发送'];

    }
}
