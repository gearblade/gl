<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
$this->registerJsFile('/im/js/jquery.json.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/jquery.cookie.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/console.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/config.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/comet.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/chat.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/swfupload.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/swfupload.queue.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/fileprogress.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerJsFile('/im/js/handlers.js',['depends'=>'backend\assets\AdminLteAsset']);
$this->registerCssFile('/im/css/chat.css',['depends'=>'backend\assets\AdminLteAsset']);
$token = Yii::$app->request->cookies->get('_csrf');
$user_id = Yii::$app->user->id;

$js = <<<JS
        var user_token = $user_id;
        var swfu;
        window.onload = function()
        {
            var settings = {
                flash_url: "/im/swf/swfupload.swf",
                //upload_script: '/myphoto/add_photo/',
                upload_url: "/apps/upload.php",
                post_params: {"uid": '0', 'post': 1, 'PHPSESSID': "0"},
                file_size_limit: "2MB",
                file_types: "*.jpg;*.png;*.gif",
                file_types_description: "图片文件",
                file_upload_limit: 100,
                file_queue_limit: 0,

                custom_settings: {
                    progressTarget: "fsUploadProgress",
                    cancelButtonId: "btnCancel"
                },
                debug: false,

                //Button settings
                button_image_url: "/im/img/button.png",
                button_width: "65",
                button_height: "29",
                button_placeholder_id: "upload_button",
                button_text : "<span>发送图片</span>",
                button_text_left_padding: 8,
                button_text_top_padding: 2,

                // The event handler functions are defined in handlers.js
                file_queued_handler : fileQueued,
                file_queue_error_handler : fileQueueError,
                file_dialog_complete_handler : fileDialogComplete,
                upload_start_handler : uploadStart,
                upload_progress_handler : uploadProgress,
                upload_error_handler : uploadError,
                upload_success_handler : uploadSuccess,
                upload_complete_handler : uploadComplete,
                queue_complete_handler : queueComplete,	// Queue plugin event

            };
            swfu = new SWFUpload(settings);
        };
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);

?>
<!--<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Swoole网页即时聊天</title>
    <link href="/im/css/bootstrap.css" rel="stylesheet">
    <link href="/im/css/chat.css" rel="stylesheet">

    <script src="/im/js/jquery.js"></script>
    <script src="/im/js/jquery.json.js"></script>
    <script src="/im/js/jquery.cookie.js"></script>
    <script src="/im/js/console.js"></script>
    <script src="/im/config.js" charset="utf-8"></script>
    <script src="/im/js/comet.js" charset="utf-8"></script>
    <script src="/im/js/chat.js" charset="utf-8"></script>
    <script type="text/javascript" src="/im/js/swfupload.js"></script>
    <script type="text/javascript" src="/im/js/swfupload.queue.js"></script>
    <script type="text/javascript" src="/im/js/fileprogress.js"></script>
    <script type="text/javascript" src="/im/js/handlers.js"></script>

    <script type="text/javascript">
        var swfu;
        window.onload = function()
        {
            var settings = {
                flash_url: "/im/swf/swfupload.swf",
                //upload_script: '/myphoto/add_photo/',
                upload_url: "/apps/upload.php",
                post_params: {"uid": '0', 'post': 1, 'PHPSESSID': "0"},
                file_size_limit: "2MB",
                file_types: "*.jpg;*.png;*.gif",
                file_types_description: "图片文件",
                file_upload_limit: 100,
                file_queue_limit: 0,
                
                custom_settings: {
                    progressTarget: "fsUploadProgress",
                    cancelButtonId: "btnCancel"
                },
                debug: false,

                //Button settings
                button_image_url: "/im/img/button.png",
                button_width: "65",
                button_height: "29",
                button_placeholder_id: "upload_button",
                button_text : "<span>发送图片</span>",
                button_text_left_padding: 8,
                button_text_top_padding: 2,

                // The event handler functions are defined in handlers.js
                file_queued_handler : fileQueued,
                file_queue_error_handler : fileQueueError,
                file_dialog_complete_handler : fileDialogComplete,
                upload_start_handler : uploadStart,
                upload_progress_handler : uploadProgress,
                upload_error_handler : uploadError,
                upload_success_handler : uploadSuccess,
                upload_complete_handler : uploadComplete,
                queue_complete_handler : queueComplete	// Queue plugin event
            };
            swfu = new SWFUpload(settings);
        };
    </script>
    <style>
        body {
            padding-top: 60px;
        }
    </style>
</head>-->
<div class="container">
    <div class="row clearfix">
        <div class="col-md-5 column">
            <div class="box box-navy direct-chat direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">XQ1023</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="direct-chat-messages" id="chat-messages">

                    </div>
                </div>

                <div class="box-footer">
                    <div id="chat-tool" style="padding-left:10px;height:30px;border:0px solid #ccc;background-color:#F5F5F5;">
                        <div style="float: left; width: 140px;">
                            <select id="userlist" style="float: left; width: 90px;">
                                <option value=0>所有人</option>
                            </select>
                            <!-- 聊天表情 -->
                            <a onclick="toggleFace()" id="chat_face" class="chat_face">
                                <img src="/im/img/face/15.gif"/>
                            </a>
                        </div>
                        <div style="float: left; width: 200px;height: 25px;">
                            <span id="upload_button" style="background-color: #f5f5f5;"></span>
                            <span style="display: none"><input id="btnCancel" style="height: 25px;" type="button" value="取消上传" onclick="swfu.cancelQueue();" disabled="disabled" /></span>
                        </div>
                    </div>
                    <div id="show_face" class="show_face">
                    </div>
                    <!--聊天表情弹出层结束-->

                    <!--发送消息区-->
                    <div id="input-msg" >
                    <form id="msgform" >
                        <!--<select  style="margin-bottom:8px" id="userlist">
                            <option value=0>所有人</option>
                        </select>-->
                        <div class="input-group">
                            <input type="text"  id="msg_content" placeholder="Type Message ..." class="form-control"  contentEditable="true">
                          <span class="input-group-btn">
                            <a onclick="sendMsg($('#msg_content').val(), 'text');"  class="btn btn-warning btn-flat">Send</a>
                          </span>
                            <div style="float: left; width: 200px;height: 25px;">
                                <span id="upload_button" style="background-color: #f5f5f5;"></span>
                                <span style="display: none"><input id="btnCancel" style="height: 25px;" type="button" value="取消上传" onclick="swfu.cancelQueue();" disabled="disabled" /></span>
                            </div>
                        </div>
                    </form>
                        </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        <div class="col-md-3 column ">

            <?= backend\widgets\friend\FriendWidget::widget()?>
            <div class="thumbnail">
                <div class="caption" id="userlist"></div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <!--主聊天区-->
            <div id="chat-column" class=" well">
                <div  style="border:0px solid #ccc;">
                    <div class="message-container">
                    </div>
                </div>
            </div>
            <div id="left-column" class="span8">
                <div class="well c-sidebar-nav">
                    <ul class="nav nav-list">
                        <li class="nav-header">Chats</li>
                        <li class="active"><a href="javascript:void(0)">In Room</a>
                        </li>
                    </ul>
                    <ul id="left-userlist">
                    </ul>
                    <div style="clear: both"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- /container -->
    <div id="msg-template" style="display: none">
        <!--<div class="direct-chat-msg right">
            <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
            </div>

            <img class="direct-chat-img" src="/adminlte/dist/img/user3-128x128.jpg" alt="message user image">
            <div class="direct-chat-text">
                You better believe it!
            </div>

        </div>-->
        <div class="message-container direct-chat-msg">
            <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left"></span>
                <span class="direct-chat-timestamp pull-right"><div class="msg-time"></div></span>
            </div>
            <div class="userpic"></div>
            <div class="message">
                <span class="user"></span>

                <div class="cloud cloudText">
                    <div style="" class="cloudPannel">
                        <div class="sendStatus"></div>
                        <div class="cloudBody">
                            <div class=" direct-chat-text"></div>
                        </div>
                        <div class="cloudArrow "></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- / -->
</div>


