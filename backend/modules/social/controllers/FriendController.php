<?php

namespace backend\modules\social\controllers;

use common\models\Friend;
use common\models\search\FriendSearch;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * RESTful api使用
 * FriendController implements the CRUD actions for Friend model.
 */
class FriendController extends ActiveController
{
    public $modelClass = 'common\models\Friend';

    /**
     * 指定资源
     * @return array
     */
    public function fields()
    {
        return [
        ];
    }

    /**
     * 设置响应格式
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;//Response::FORMAT_XML;
        return $behaviors;
    }

    /**
     * 自定义动作
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        // 禁用"delete" 和 "create" 动作
        unset($actions['delete'], $actions['create'],$actions['view']);

        // 使用"prepareDataProvider()"方法自定义数据provider
        $actions['index']['prepareDataProvider'] = [$this, 'actionIndex'];

        return $actions;
    }

    /**
     * 返回当前用户好友列表
     * Lists all Friend models.
     * @return \yii\data\ActiveDataProvider
     */
    public function actionIndex(){
        $searchModel = new FriendSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $dataProvider;
    }

    /**
     * 展示详情
     * Displays a single Friend model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //禁止访问其他用户好友
        $model = Friend::findOne(['user_id'=>Yii::$app->user->id,'id'=>$id]);
        return $model;
    }

    /**
     * 创建好友
     * Creates a new Friend model.
     * @return mixed
     */
    public function actionCreate($id)
    {
        return $this->actionFollow($id);
    }
    /**
     * 删除好友
     * Delete a new Friend model.
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->actionFollow($id);
    }

//    /**
//     * 更新好友
//     * Updates an existing Friend model.
//     * If update is successful, the browser will be redirected to the 'view' page.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }


    /**
     * Follow a new Friend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionFollow($id)
    {
        $result = ['status'=>'success'];
        $model=Friend::findOne(['user_id'=>Yii::$app->user->id,'friend_id'=>$id]);
        if(null==$model){  // 未关注
            $model = new Friend();
            $model->user_id = Yii::$app->user->id;
            $model->friend_id = $id;

            if ($model->save()) {
                $result['name']='followed';

            } else {
                $result = ['status'=>'error'];

            }
        }else{   // 已关注
            /*$model->user_id = Yii::$app->user->id;
            $model->friend_id = $id;*/

            $model->delete();
            $result['name']='follow';
        }
        return json_encode($result);
    }

    /**
     * Finds the Friend model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Friend the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Friend::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
