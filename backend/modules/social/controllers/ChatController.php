<?php

namespace  backend\modules\social\controllers;

use yii\web\Controller;

/**
 * Default controller for the `social` module
 */
class ChatController extends Controller
{
    public $layout = '//main_nav';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionMain()
    {
        return $this->render('main');
    }
}
