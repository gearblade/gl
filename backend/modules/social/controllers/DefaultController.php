<?php

namespace backend\modules\social\controllers;

use yii\rest\ActiveController;
use yii\web\Controller;

/**
 * Default controller for the `social` module
 */
class DefaultController extends ActiveController
{
    public $modelClass = 'common\models\Friend';
}
