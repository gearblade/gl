<?php
/**
 * Created by PhpStorm.
 * User: xq
 * Date: 16-5-19
 * Time: 上午7:11
 */
namespace backend\widgets\friend;

use backend\widgets\box\assets\BoxAsset;
use common\models\Document;
use common\models\Friend;
use yii\base\Widget;
use yii\db\ActiveRecord;

class FriendWidget extends Widget
{
    public $css = ['warper'=>'box-widget','title'=>'with-border','body'=>'box-profile'];
    public $model ;
    public $activeRecord ;
    public $cate ;
    public $where ;
    public $liNum = 10;
    public $pic;
    public $type;
    public $title;
    public $url;

    /**
     * 初始化
     * @see \yii\base\Object::init()
     */
    public function init(){
        parent::init();
    }


    public function run(){
        //$this->registerClientScript();

        if ($this->model === null) {
            $this->model = new Friend();
            $user_id = \Yii::$app->user->id;
            $this->activeRecord = $this->model->find()
                ->where(['user_id'=>$user_id])
                ->all();
            //var_dump($this->activeRecord);die();
            $userInfo = [];
            foreach( $this->activeRecord as $k=>$v){
                $userInfo[] = $v->getUser()->asArray()->one();
            }

            //echo 'hehhe';die();
            //$this->activeRecord = Cate::find()->where(['status'=>Cate::$STATUS_AOLLOW])->orderBy(['level' => SORT_DESC])->all();//new Room();//'Hello World';
        }else{
            //$this->model = new ActiveRecord();
            $this->model = new $this->model();
            $this->activeRecord = $this->model
                ->find()
                ->where($this->where)
                ->orderBy(['level' => SORT_DESC])->limit($this->liNum)->all();
        }

        /*$renderArray = [
            'model'=>$this->model,
            'ac'=>$this->activeRecord,
            'css'=>$this->css,
            'title'=>$this->title,
            'url'=>$this->url,
            'cate'=>$this->cate,
        ];*/

         return $this->render('friend_list',['userInfo' =>$userInfo]);


    }
    public function registerClientScript()
    {
        BoxAsset::register($this->view);
        //$script = "FormFileUpload.init();";
        //$this->view->registerJs($script, View::POS_READY);
    }
}