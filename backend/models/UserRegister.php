<?php

namespace backend\models;
use dektrium\user\helpers\Password;

use dektrium\user\models\User;
use Yii;
use dektrium\user\models\LoginForm;
use dektrium\user\models\Profile;
use dektrium\user\models\Token;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $user_id
 * @property string $email
 * @property string $password
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $status
 * @property string $password_reset_token
 *
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class UserRegister extends \yii\db\ActiveRecord
{
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    /*public function scenarios()
    {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'register' => ['username', 'email', 'password'],
            'connect'  => ['username', 'email'],
            'create'   => ['username', 'email', 'password'],
            'update'   => ['username', 'email', 'password'],
            'settings' => ['username', 'email', 'password'],
            'register_tel' => ['username', 'tel', 'password'],
        ]);
    }*/
    public function scenarios()
    {
        return [
            self::SCENARIO_LOGIN => ['user_id', 'password'],
            self::SCENARIO_REGISTER => ['user_id', 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
    public $password;
    public $msg ;
    protected $user;
    public $rememberMe = false;
    public $loginStr;
    public $finduser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'loginTrim' => ['login', 'trim'],
            [['username', 'password_hash', 'auth_key',  ], 'required'],
            [['confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'status','msg'], 'integer'],
            [['username',  'unconfirmed_email'], 'string', 'max' => 100],
            [['user_id'], 'string', 'max' => 100],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['password_reset_token'], 'string', 'max' => 256],
            //[['email'], 'unique'],
            [['username'], 'unique'],
            'user_idUnique'   => [
                'user_id',
                'unique',
                'message' => \Yii::t('user', 'This user_id has already been taken')
            ],
            //[['username', 'user_id', 'password',  'msg',  ], 'required','on'=>'register'],
            [['msg',  ], 'required'],
            'passwordValidate' => [
                'password',
                function ($attribute) {
                    if ($this->user === null || !Password::validate($this->password, $this->finduser->password_hash)) {
                        $this->addError($attribute, Yii::t('user', 'Invalid login or password'));
                    }
                },'on' => 'login'
            ],
            /*'msgValidate' => [
                'msg',
                function ($attribute, $params){
                    //echo Yii::$app->redis->get('tel'.Yii::$app->user->id);
                    if ($this->msg!= Yii::$app->redis->get('tel'.Yii::$app->user->id) ){
                        $this->addError($attribute, '验证码不对');
                    }
                },'on' => 'register'
            ],*/
            'rememberMe' => ['rememberMe', 'boolean'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '名称',
            'tel' => '手机号',
            'msg' => '短信验证码',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'password' => '密码',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'flags' => 'Flags',
            'status' => 'Status',
            'password_reset_token' => 'Password Reset Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    public function login()
    {

        if ($this->validate()) {
            return Yii::$app->getUser()->login($this->user,  $this->rememberMe);
        } else {

            return false;
        }
    }

    public function beforeValidate()
    {
       /* echo 33;
        echo $this->login;die();*/
        if (parent::beforeValidate()) {

            $this->finduser = $this->findUserByUsernameOrTel($this->loginStr);
            if($this->finduser == null){
                $this->user =null;
                return true;
            }
            $user = new User();
            $user->findIdentity($this->finduser->id);
            //$user->username = $finduser->username;
            //$user->pa = $finduser->username;
            $this->user = $user;
            return true;
        } else {
            return false;
        }
    }
    public function findUserByUsernameOrTel($usernameOrEmail)
    {
       /* if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return $this->findUserByEmail($usernameOrEmail);
        }*/
        echo $usernameOrEmail;
        return $this->findUserByUsername($usernameOrEmail);
    }
    public function findUserByTel($tel)
    {
        return $this->findOne(['tel' => $tel]);
    }

    public function findUserByUsername($username)
    {
        return $this->findOne(['username' => $username]);
    }

    public function register($info)
    {
        $this->scenario = 'register';

        $this->attributes = $info;   // 用户模型
        $this->username = $info['username'];   // 用户名
        /*var_dump($info);
var_dump($this->attributes);die();*/
        $this->password_hash = Password::hash($this->password);
        $this->confirmed_at = time();
        $this->auth_key = \Yii::$app->security->generateRandomString();
        $this->created_at = time();
        $this->updated_at = time();

        if ($this->save()) {
                $profile = new Profile();   //个人信息
                $profile->user_id = $this->id;
                $profile->gravatar_email = '/images/icon/default_m.png';

                if ($profile->save()) {
                    $token = new Token();   //token
                    $token->user_id = $this->id;
                    $token->type = Token::TYPE_CONFIRMATION;
                    if($token->save()){
                        $auth = \Yii::$app->authManager;  //权限
                        $role = $auth->createRole('学生');
                        $auth->assign($role, $this->id);
                        //$this->trigger(self::EVENT_AFTER_REGISTER, $event);
                        $login = \Yii::createObject(LoginForm::className());

                        $login->login = $this->user_id;
                        $login->password = $this->password;
                        $login->login();

                        return true;
                    }else{
                        var_dump($token->getErrors());
                        return false;
                    }

                }else{
                    var_dump($profile->getErrors());
                    return false;
                }

                //return $this->redirect(['/user/settings/profile', 'modal' => 1]);
                /* return $this->render('model', [
                     'title'  => \Yii::t('user', 'Your account has been created'),
                     //'module' => $this->module,
                 ]);*/
            }else{
            var_dump($this->getErrors());
            return false;
        }
    }

}
