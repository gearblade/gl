<?php
/**
 * This is mongodb model
 */
namespace backend\models;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\mongodb\file\ActiveRecord;
use yii\mongodb\file\Download;
use Yii;
use backend\modules\dbms\models\DataFile;
use yii\base\Object;

/**
 * This is the model class for collection "data".
 *
 * @property \MongoId|string $_id
 * @property mixed $title
 * @property mixed $author
 * @property mixed $uploadtime
 * @property mixed $course_id
 * @property mixed $score
 * @property mixed $type
 * @property mixed $size
 * @property mixed $summary
 * @property mixed $status
 * @property mixed $publicity
 * @property mixed $files_id
 * @property mixed $number
 * @property mixed $downloadtimes
 */
class Data extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['gl', 'data'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'author',
            'uploadtime',
            'course_id',
            'score',
            'type',
            'size',
            'summary',
            'status',
            'publicity',
            'files_id',
            'number',
            'downloadtimes',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'author', 'uploadtime', 'course_id', 'score', 'type', 'size', 'summary', 'status', 'publicity', 'files_id', 'number', 'downloadtimes'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'title' => '标题',
            'author' => '上传者',
            'uploadtime' => '上传时间',
            'course_id' => 'Book ID',
            'score' => '分数',
            'type' => '文档类型',
            'size' => '文档大小',
            'summary' => '文档概要',
            'status' => '状态',
            'publicity' => '可见范围',
            'files_id' => 'Files ID',
            'number' => 'Number',
            'downloadtimes' => '下载次数',
        ];
    }
    
    /**
     * 保存上传资料的相关信息
     * @param unknown $content 字段
     * @param unknown $post	post内容
     * @return boolean
     */
    public function saveData($content, $post)
    {
    	$this->title = $content['title'];
    	$this->size = $content['size'];
    	$this->type = $content['type'];
    	$this->files_id = $content['files_id'];
    	$this->author = Yii::$app->user->identity->username;
    	$this->uploadtime = time();
    	$this->summary = $post['Data']['summary'];
    	$this->publicity = $post['Data']['publicity'];
    	$this->score = 0;	//默认评分
    	$this->course_id = $content['course_id'];
    	$this->status = 0;
    	$this->downloadtimes = 0 ;
    	if($this->save())
    	{
    		return true;
    	}else {
    		return false;
    	}
    }
    
    /**
     * 得到分页的数据，在前端显示
     * @param unknown $curPage  当前页面 int
     * @param number $pageSize  单个页面信息的条数 int
     * @return multitype:number multitype: unknown |unknown
     */
    
    public function getData($curPage, $pageSize = 10)
    {
    	
    	$query = $this->find()->orderBy(['uploadtime' => SORT_DESC]);
    	$data['count'] = $query->count();
    	if(!$data['count'])
    	{
    		return ['count'=>0, 'curPage'=>$curPage, 'pageSize'=>$pageSize, 'start'=>0,
    				'end' => 0, 'data' => [],
    		];
    	}
    	//超过实际页数，不去当前页curPage
    	$curPage = (ceil($data['count']/$pageSize) < $curPage)?ceil($data['count']/$pageSize):$curPage;
    	 
    	$data['curPage'] = $curPage;
    	 
    	$data['pageSize'] = $pageSize;
    	//起始页
    	$data['start'] = ($curPage-1)*$pageSize+1;
    	//末页
    	$data['end'] = (ceil($data['count']/$pageSize) == $curPage)?$data['count']:$curPage*$pageSize;
    	 
    	$data['data'] = $query->offset(($curPage-1) * $pageSize)
    	->limit($pageSize)
    	->asArray()
    	->all();
    	return $data;
    }
    
    /**
     * 判断文件是否存在
     * @param unknown _id 
     */
    public function isExists($_id)
    {
    	return $this->find()->where(['_id' => $_id])->count();
    }
    /**
     * 通过文件的_id得到文件的title
     * @param unknown _id
     */
    public function getTitle($_id)
    {
    	//名称加后缀名
    	$title = $this->find()->select(['title','type'])->where(['_id' => $_id])->asArray()->one();
    	$result = $title['title'].'.'.$title['type'];
    	print_r($title);
    	return $result;
    }
    
    /**
     * 根据files_id直接下载对应文件
     * @param unknown _id
     * @param unknown $title
     */
    public function download($_id, $title)
    {
    	
    	$_id = $this->find()->select(['files_id'])->where(['_id'=>$_id])->asArray()->one()['files_id'];
    	$files = new Download();
    	//$files->
    	//DataFile::findOne($_id);
    	print_r($files);
  /*  	$result = Yii::$app->mongodb
			    	  ->getFileCollection()
			    	  ->createDownload($files)
			    	  ->toFile('d:\\'.$title);
   */ 	if ($result=1)
    	{
    		return true;
    	}else{
    		return false;
    	}
    }
    
    /**
     * 增加下载次数
     * @param unknown $_id
     */
    public function upCounter($_id)
    {
    	$counter = $this->findOne($_id);
    	$counter->updateCounters(['downloadtimes' => 1]);
    }
    public function getSingleData($_id)
    {
    	$data = $this->find()->where(['_id' => $_id])->asArray()->one();
    	return $data;
    }
     
}
