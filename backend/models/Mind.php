<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mind".
 *
 * @property integer $id
 * @property string $username
 * @property string $content
 * @property integer $user_id
 * @property string $create_at
 * @property string $img
 * @property integer $status
 * @property integer $likes
 */
class Mind extends \yii\db\ActiveRecord
{
    const STATUS_AOLLOW=1;
    //const STATUS_DEFAULT=1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mind';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'user_id', 'img','title'], 'required'],
            [['content', 'img','title'], 'string'],
            [['user_id', 'status', 'likes'], 'integer'],
            [['create_at'], 'safe'],
            [['username','title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'title' => 'Title',
            'content' => 'Content',
            'user_id' => 'User ID',
            'create_at' => 'Create At',
            'img' => 'Img',
            'status' => 'Status',
            'likes' => 'Likes',
        ];
    }
}
