<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $detail
 * @property string $school
 * @property string $house
 * @property string $room
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'detail', 'school', 'house', 'room'], 'required'],
            [['user_id'], 'integer'],
            [['detail'], 'string'],
            [['school', 'house', 'room'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'detail' => 'Detail',
            'school' => 'School',
            'house' => 'House',
            'room' => 'Room',
        ];
    }
}
