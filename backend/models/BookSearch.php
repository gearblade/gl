<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Book;

/**
 * BookSearch represents the model behind the search form about `backend\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            [['name', 'publish', 'author', 'edition', 'pic', 'background', 'create_at', 'publish_at', 'publish_num', 'max'], 'safe'],
            [['price'], 'number'],
        ];
    }
    public $max;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 18,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'create_at' => $this->create_at,
            'publish_at' => $this->publish_at,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->orFilterWhere(['like', 'publish', $this->publish])
            ->andFilterWhere(['like', 'author', $this->author])

            ->andFilterWhere(['like', 'edition', $this->edition])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'background', $this->background])
            ->andFilterWhere(['like', 'publish_num', $this->publish_num]);
        if(!empty($this->max)){
            $query->orFilterWhere(['like', 'name', $this->max])
                ->orFilterWhere(['like', 'author', $this->max])
                ->orFilterWhere(['like', 'publish', $this->max])
                ->orFilterWhere(['like', 'background', $this->max]);
        }
        return $dataProvider;
    }

    public function searchTag($params,$tags){
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 18,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        /*$query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'create_at' => $this->create_at,
            'publish_at' => $this->publish_at,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->orFilterWhere(['like', 'publish', $this->publish])
            ->andFilterWhere(['like', 'author', $this->author])

            ->andFilterWhere(['like', 'edition', $this->edition])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'background', $this->background])
            ->andFilterWhere(['like', 'publish_num', $this->publish_num]);
        if(!empty($this->max)){
            $query->orFilterWhere(['like', 'name', $this->max])
                ->orFilterWhere(['like', 'author', $this->max])
                ->orFilterWhere(['like', 'publish', $this->max])
                ->orFilterWhere(['like', 'background', $this->max]);
        }*/
        foreach($tags as $tag){

            $query->where(['id'=>$tag->book_id]);
        }
        //echo $query->sql;die()

        return $dataProvider;
    }
}
