<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "textbook".
 *
 * @property integer $id
 * @property string $name
 * @property string $publish
 * @property string $author
 * @property string $edition
 * @property string $pic
 * @property string $background
 * @property double $price
 * @property string $create_at
 * @property string $update_at
 */
class Textbook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'textbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'publish', 'author', 'edition', 'pic', 'price','publish_at'], 'required'],
            [['pic', 'background','publish_at','publish_num'], 'string'],
            [['price'], 'number'],
            [['create_at', ], 'safe'],
            [['name', 'publish', 'author'], 'string', 'max' => 255],
            [['edition'], 'string', 'max' => 250],
            [['publish_num'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'publish' => 'Publish',
            'author' => 'Author',
            'edition' => 'Edition',
            'pic' => 'Pic',
            'background' => 'Background',
            'price' => 'Price',
            'create_at' => 'Create At',
            'publish_num' => 'Publish Num',
            'publish_at' => 'Publish At',
        ];
    }
}
