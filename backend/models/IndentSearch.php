<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Indent;

/**
 * IndentSearch represents the model behind the search form about `backend\models\Indent`.
 */
class IndentSearch extends Indent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'book_id', 'user_id', 'status', 'sender_id'], 'integer'],
            [['address', 'ext', 'create_at', 'school', 'house', 'room'], 'safe'],
            [['money'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Indent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'book_id' => $this->book_id,
            'user_id' => $this->user_id,
            'create_at' => $this->create_at,
            'status' => $this->status,
            'money' => $this->money,
            'sender_id' => $this->sender_id,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'ext', $this->ext])
            ->andFilterWhere(['like', 'school', $this->school])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'room', $this->room]);

        return $dataProvider;
    }
}
