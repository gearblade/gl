<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $name
 * @property string $publish
 * @property string $author
 * @property string $edition
 * @property string $pic
 * @property string $background
 * @property double $price
 * @property string $create_at
 * @property string $publish_at
 * @property string $publish_num
 * @property integer $amount
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'publish', 'author', 'edition', 'pic', 'price', 'publish_at'], 'required'],
            [['pic', 'background','tag'], 'string'],
            [['price'], 'number'],
            [['create_at', 'publish_at'], 'safe'],
            [['amount'], 'integer'],
            [['name', 'publish', 'author','tag'], 'string', 'max' => 255],
            [['edition'], 'string', 'max' => 250],
            [['publish_num'], 'string', 'max' => 110],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'publish' => 'Publish',
            'author' => 'Author',
            'edition' => 'Edition',
            'pic' => 'Pic',
            'background' => 'Background',
            'price' => 'Price',
            'create_at' => 'Create At',
            'publish_at' => 'Publish At',
            'publish_num' => 'Publish Num',
            'amount' => 'Amount',
        ];
    }

    public function excelInto()
    {
        //require_once '/libs/PHPExcel-1.8.0/Classes/PHPExcel.php';

        //注：在yii中，也可以直接Yii::import(“application.lib.PHPExcel.*”);


        if ($_POST['leadExcel'] == "true") {
            $filename = $_FILES['inputExcel']['name'];
            $tmp_name = $_FILES['inputExcel']['tmp_name'];
            $msg = $this->uploadFile($filename, $tmp_name);
            return $msg;
        }
    }
    //导入Excel文件
    public function uploadFile($file,$filetempname)
        {
            set_include_path('.' . PATH_SEPARATOR . Yii::$app->basePath . '/lib/PHPExcel/Classes/');
            //自己设置的上传文件存放路径
            $filePath = 'upFile/';
            $str = "";
            //下面的路径按照你PHPExcel的路径来修改
            require_once 'PHPExcel.php';
            require_once 'PHPExcel/IOFactory.php';
            require_once 'PHPExcel/Reader/Excel5.php';

            //注意设置时区
            $time=date("y-m-d-H-i-s");//去当前上传的时间
            //获取上传文件的扩展名
            $extend = strrchr ($file,'.');
            //上传后的文件名
            $name=$time.$extend;
            $uploadfile = Yii::$app->basePath.'/web/excel/'.$name;//$filePath.$name;//上传后的文件名地址
            //move_uploaded_file() 函数将上传的文件移动到新位置。若成功，则返回 true，否则返回 false。
            $result= move_uploaded_file($filetempname,$uploadfile);//假如上传到当前目录下
            //echo $result;
            if($result) //如果上传文件成功，就执行导入excel操作
            {
                //include "conn.php";
                $objReader = \PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
                $objPHPExcel = $objReader->load($uploadfile);
                $sheet = $objPHPExcel->getSheet(0);

                $highestRow = $sheet->getHighestRow();           //取得总行数
                $highestColumn = $sheet->getHighestColumn(); //取得总列数


                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();

               /* echo 'highestRow='.$highestRow;
                echo "<br>";*/
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); //总列数
               /* echo 'highestColumnIndex='.$highestColumnIndex;
                echo "<br>";*/
                $headtitle=array();

                for ($row = 1;$row <= $highestRow;$row++)
                {
                    $strs=array();
                    //注意highestColumnIndex的列数索引从0开始
                    for ($col = 0;$col < $highestColumnIndex;$col++)
                    {
                        $strs[$col] =$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                        //$this->attributes = $strs;



                        //var_dump($strs[0][1]);

                    }
                     $this->name = htmlspecialchars_decode($strs[0]);

                         $this->publish = htmlspecialchars_decode($strs[1]);
                         $this->author = htmlspecialchars_decode($strs[2]);
                         $this->edition =  htmlspecialchars_decode($strs[3]);
                         $this->pic = $strs[4];
                         $this->background = htmlspecialchars_decode($strs[5]);
                         $this->price = $strs[6];
                         //var_dump($this->price);
                         $this->publish_at = $strs[8];
                         $this->publish_num = htmlspecialchars_decode($strs[9]);
                    $this->insert();



                   /*
                    $sql = "INSERT INTO te(`1`, `2`, `3`, `4`, `5`) VALUES (
                        '{$strs[0]}',
                        '{$strs[1]}',
                        '{$strs[2]}',
                        '{$strs[3]}',
                        '{$strs[4]}')";*/

                    //die($sql);
                    /*if(!mysql_query($sql))
                    {
                        return false;
                        echo 'sql语句有误';
                    }*/
                    $msg ='导入成功';
                }
            }
            else
            {
                $msg = "导入失败！";
            }
            return $msg;
        }

    public function setTag($content){
        $oldTags =Tag::findAll(['book_id'=>$this->id]);

        foreach($oldTags as $v){
            $v->delete();
        }

        $tags = explode(',',$content);


        $tag = new Tag();
        $book_id = $this->id;
        foreach($tags as $v){
            $tag = new Tag();
            $tag->book_id = $book_id;
            $tag->tag = $v;
            $tag->save();
        }


    }

    public function getTags(){
        //$tag = new Tag();
        return $tag =Tag::findAll(['book_id'=>$this->id]);

    }

    public function getTag(){
        $tag =Tag::findAll(['book_id'=>$this->id]);
        $tagarr =[];
        foreach($tag as $v){
            $tagarr[] = $v->tag;
        }
        $tags = implode(',',$tagarr);

        return $tags;
    }

}
