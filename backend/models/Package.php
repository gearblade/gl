<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $book_id
 * @property integer $status
 * @property string $create_at
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }
    const STATUS_DELETE = 0;    //失效
    const STATUS_INDENT = 1;    //已下单
    const STATUS_COLLECT = 2;   // 收藏
    const STATUS_RECEIVE = 3;   //已接收
    const STATUS_TIME = 4;   //超期
    const STATUS_RETURN = 5;    //已返还
    const STATUS_LOSE = 6;      //丢失


    public $statusview = ['丢失','已下单','书袋中','手中','请返还','借过'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id', 'status'], 'required'],
            [['user_id', 'book_id', 'status'], 'integer'],
            [['create_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'book_id' => 'Book ID',
            'status' => 'Status',
            'create_at' => 'Create At',
        ];
    }
    public function getUsername(){
        $user = \dektrium\user\models\User::findOne(['id'=>$this->user_id]);
        if(!empty($user)){
            return $user->profile->name;
        }
    }

    public function getBookname(){
        $book = Book::findOne(['id'=>$this->book_id]);
        if(!empty($book)){
            return $book->name;
        }
    }
    public function getStatusView(){
        return $this->statusview[$this->status];
        //$view = ['']
    }


}
