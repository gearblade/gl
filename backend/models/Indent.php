<?php

namespace backend\models;

use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "indent".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $user_id
 * @property string $address
 * @property string $ext
 * @property string $create_at
 * @property integer $status
 * @property string $school
 * @property string $house
 * @property string $room
 * @property double $money
 * @property integer $sender_id
 */
class Indent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indent';
    }

    const UnderDeal = 1;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'user_id',  'school', 'house', 'room','tel','costomer' ], 'required'],
            [['book_id', 'user_id', 'status', 'sender_id'], 'integer'],
            [['address',], 'string'],
            [['create_at'], 'safe'],
            [['money','tel'], 'number'],
            [['ext', 'school', 'house', 'room','costomer'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'user_id' => 'User ID',
            'address' => 'Address',
            'ext' => '备注',
            'create_at' => 'Create At',
            'status' => 'Status',
            'school' => 'School',
            'house' => 'House',
            'room' => 'Room',
            'money' => 'Money',
            'sender_id' => 'Sender ID',
        ];
    }

    public function getUser(){
        $user = User::findOne(['id'=>$this->user_id]);
        return $user;
    }

    public function getUsername(){
        return $this->User->username;
    }

    public function getBook(){
        return Book::findOne(['id'=>$this->book_id]);
    }

    public function getBookname(){
        return $this->book->name;
    }
}
