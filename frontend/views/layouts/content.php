<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use yii\bootstrap\Modal;
?>
<div class="content-wrapper wrapper-images" style="">
    <?php if(!isset($this->params['index'])){ ?>
    <section class="content-header container" >
        <?php if (isset($this->blocks['content-header'])) { ?>
            <!--<h1><?= $this->blocks['content-header'] ?></h1>-->
        <?php } else { ?>
            <!--<h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>-->
        <?php } ?>


    </section>
    <?php } ?>

    <section class="content" >
        <?= Alert::widget() ?>
        <?= $content ?>
        <?php
        Modal::begin([
            'id' => 'create-modal',
            'header' => '<h4 class="modal-title"></h4>',
            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
        ]);

        Modal::end();

        ?>
    </section>

</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>版本</b> beta 0.8
    </div>
    <strong> <a href="http://.com">归锋工作室</a></strong> 技术支持
</footer>
