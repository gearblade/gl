<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use backend\widgets\category\CategoryWidget;
/* @var $this \yii\web\View */
/* @var $content string */

?>

<header class="main-header">


    <nav class="navbar navbar-static-top" role="navigation">
        <div class="container" style="padding-right: 15px;padding-left: 15px;">
        <div class="navbar-header">

            <a href="<?=Url::home()?>" class="navbar-brand" >科创俱乐部</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <?php
        /*if(Yii::$app->user->isGuest) {
            $menuItemsCenter = [
                ['label' => '登录', 'url' => ['/site/login']],
                ['label' => '注册', 'url' => ['/site/registe']],

            ];
        }else{*/


        $menu = new CategoryWidget(['precate_name'=>'document']);
        $menuItemsCenter = $menu->getCate();
        //var_dump($menuItemsCenter);die();
           /* $menuItemsCenter = [
                ['label' => '首页','url' => ['/site/index']],
                ['label' => '概况', 'url' => ['/frontend/info']],
                //['label' => '研究领域', 'url' => ['/site/about']],
                ['label' => '科研成果', 'url' => ['/achievement']],
                ['label' => '研究生风采', 'url' => ['/useradmin/loader/create']],
                ['label' => '招生', 'url' => ['/help/index']],
                ['label' => '联系我们', 'url' => ['/site/contact']],
            ];
        var_dump($menuItemsCenter);die();*/
        //}

        echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav'],
            'encodeLabels' => false,
            'items' => $menuItemsCenter,
        ]);

        ?>
            </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <?php if(Yii::$app->user->isGuest) { ?>
                    <li class="">
                        <a href="/user/login" >
                            登录
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="/register" >
                            注册
                        </a>
                    </li>
                <?php }else{
                    //$social = new \vendor\social\chat();
                    //$social->login();
                     //die();
                    //$name = Yii::$app->user->identity->username ;
                    //$js = "var name = {$name};";

                    //$this->registerJs($js,['position'=>\yii\web\View::POS_LOAD]);

                   /* $this->registerJsFile('/chat/js/domain.js',
                        ['depends'=>'backend\assets\AdminLteAsset',
                            'position'=>\yii\web\View::POS_END]);*/

                    ?>

                <!-- Tasks: style can be found in dropdown.less -->

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::$app->user->identity->profile->gravatar_email ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">

                        <!-- User image -->


                        <li >
                            <div class="box box-widget ">
                                <!-- Add the bg color to the header using any of the bg-* classes -->

                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="/package/">书袋 <span class="pull-right badge bg-aqua">5</span></a></li>
                                        <?php if(\Yii::$app->user->can('图书添加')){
                                            ?><li><a href="/book/admin">后台 </a></li>
                                        <?php
                                        } ?>
                                        <li><a href="/user/settings/profile">设置 </a></li>
                                        <li><?= Html::a(
                                                '退出',
                                                ['/site/logout'],
                                                ['data-method' => 'post', ]
                                            ) ?></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->

                    </ul>
                </li>
                <?php } ?>
                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
            </div>
    </nav>
    <div class="" style="">
        <div class=" ">

            <ul class="panel quick-menu clearfix">


                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa  fa-bar-chart-o"></i>机器人</a>
                </li>
                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa  fa-bar-chart-o"></i>交通</a>
                </li>
                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa  fa-bar-chart-o"></i>开源</a>
                </li>
                <li class="col-sm-1 col-xs-4">

                    <a href="#"> <div class="icon">
                            <i class="fa fa-edit"></i>
                        </div>工程</a>
                </li>
                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa  fa-bar-chart-o"></i>统计</a>
                </li>

                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa fa-bell-o"></i>消息</a>
                </li>

                <li class="col-sm-1 col-xs-4">
                    <a href="#"><i class="fa fa-cogs"></i>设置</a>
                </li>
                <li class="col-sm-1 col-xs-4">
                    <a href=""><i class="fa  fa-question"></i>帮助</a>
                </li>

            </ul>


        </div>

    </div>
</header>
