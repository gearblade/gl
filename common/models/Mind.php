<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;
use backend\models\Tag;

/**
 * This is the model class for table "mind".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $user_id
 * @property string $create_at
 * @property string $img
 * @property integer $status
 * @property integer $type
 * @property integer $likes
 * @property integer $oppose
 * @property string $icon
 * @property string $tag
 * @property integer $cate
 * @property integer $relevant_id
 */
class Mind extends \yii\db\ActiveRecord
{
    const TYPE_TEAM = 3;
    const TYPE_QUESTION = 2;
    const TYPE_MIND = 1;
    const TYPE_DOCUMENT = 4;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mind';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'user_id',/* 'cate'*/], 'required'],
            [['content', 'img', 'icon','tag'], 'string'],
            [['user_id', 'status', 'likes', 'oppose', 'cate','type','relevant_id'], 'integer'],
            [['create_at'], 'safe'],
            [['title'], 'string', 'max' => 200],
            //[['username'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '主题',
            'tag' => '标签',
            'content' => '描述',
            'user_id' => 'User ID',
            'create_at' => 'Create At',
            'img' => '配图',
            'status' => 'Status',
            'likes' => '赞',
            'oppose' => '反',
            'icon' => 'Icon',
            'cate' => 'Cate',
        ];
    }
    public function getTeam(){
        return Team::findOne(['mind_id'=>$this->id]);

    }
    public function getUser(){
        return User::findOne(['id'=>$this->user_id]);
    }

    public function getPostCount(){
        return Post::find()->where(['mind_id'=>$this->id])->count();
    }

    public function setTag($content){
        $oldTags =Tag::findAll(['object_id'=>$this->id]);

        foreach($oldTags as $v){
            $v->delete();
        }
        $tags = explode(',',$content);
#echo $content;die();
        $tag = new Tag();
        $object_id = $this->id;
        foreach($tags as $v){
            $tag = new Tag();
            $tag->object_id = $object_id;
            $tag->tag = $v;
            $tag->save();
        }
    }

    public function getTags(){
        //$tag = new Tag();
        return $tag =Tag::findAll(['object_id'=>$this->id]);

    }

    public function getTag(){
        $tag =Tag::findAll(['object_id'=>$this->id]);
        $tagarr =[];
        foreach($tag as $v){
            $tagarr[] = $v->tag;
        }
        $tags = implode(',',$tagarr);

        return $tags;
    }
}
