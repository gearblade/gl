<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "friend".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $friend_id
 * @property integer $created_at
 */
class Friend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_id', ], 'required'],
            [['user_id', 'friend_id', ], 'integer'],
            ['friend_id', 'exist', 'targetAttribute' => 'id' ,'targetClass'=>'dektrium\user\models\user'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'user_id' => Yii::t('common', 'User ID'),
            'friend_id' => Yii::t('common', 'Friend ID'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    public function getUser()
    {
        // 客户和订单通过 Order.customer_id -> id 关联建立一对多关系
        return $this->hasOne(User::className(), ['id' => 'friend_id'])
            ->select(['username','id',])
            ->where(['status'=>\common\models\User::STATUS_ACTIVE]);
    }
}
