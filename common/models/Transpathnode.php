<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transpathnode".
 *
 * @property integer $id
 * @property integer $path_id
 * @property integer $path_order
 * @property integer $area_id
 * @property string $area
 * @property integer $destination_id
 * @property string $destination
 * @property integer $distance
 * @property integer $price
 * @property float $rate
 *
 */
class Transpathnode extends \yii\db\ActiveRecord
{

    const STATUS_AOLLOW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transpathnode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            // 'id' => 'ID',
        ];
    }

    public static $STATUS_DELETE = 0;
    public static $STATUS_AOLLOW = 1;
    public static $STATUS_CANCLE = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAindices()
    {
        return $this->hasMany(Aindex::className(), ['urls' => 'id']);
    }
    public static function getUrlsName($id)
    {
        return self::findOne(['id'=>$id]);
    }
}
