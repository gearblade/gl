<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transpath2;
use yii\web\NotFoundHttpException;

/**
 * TransPathSearch represents the model behind the search form about `common\models\TransPath`.
 */
class TranspathSearch extends Transpath2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'area_id', 'destination_id','distance','price'], 'integer'],
            [['area', 'destination'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transpath2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
       
        // throw new NotFoundHttpException("d322332d" . $this->area . $this->destination);

        $query->andFilterWhere([
            'id' => $this->id,
            'area' => $this->area,
            'area_id' => $this->area_id,
            'destination' => $this->destination,
            'destination_id' => $this->destination_id,
            'distance' => $this->distance,
            'price' => $this->price,
            'rate' => $this->rate,
            'office' => $this->office,
            'office_id' => $this->office_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'area_id', $this->area_id])
            ->andFilterWhere(['like', 'destination', $this->destination])
            ->andFilterWhere(['like', 'destination_id', $this->destination_id])
            ->andFilterWhere(['like', 'distance', $this->distance])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'rate', $this->rate])
            ->andFilterWhere(['like', 'office', $this->office])
            ->andFilterWhere(['like', 'office_id', $this->office_id]);

        return $dataProvider;
    }
}