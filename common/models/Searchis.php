<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;
use backend\models\Tag;

/**
 * This is the model class for table "searchis".
 *
 * @property integer $id
 * @property string $title
 */
class Searchis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'searchis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

}
