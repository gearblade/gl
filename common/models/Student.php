<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $password
 * @property string $name
 * @property string $sex
 * @property string $signature
 * @property string $intro
 * @property string $skill
 * @property string $birthday
 * @property string $college
 * @property string $major
 * @property string $class
 * @property string $native
 * @property string $politics
 * @property string $address
 * @property string $person_id
 * @property string $tel
 * @property string $email_t
 */
class Student extends \yii\db\ActiveRecord
{
    public static $yzmUrl ='http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG'; //登陆验证码
    public static $loginUrl = 'http://jiaowu.swjtu.edu.cn/servlet/UserLoginSQLAction';//登陆服务地址
    public static $loginPageUrl = 'http://jiaowu.swjtu.edu.cn/service/login.jsp';//登陆页面地址
    private $power = 4;
    private $mid = 6;
    public $student_id ;
    public $ranstring;  //验证码
    public $picId;      //cookie名称
    public $username; //用户名
    public static $RanstringPicPath = '/images/ranstring/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'password', 'name', 'sex', 'person_id', 'tel'], 'required'],
            [['sex', 'skill', 'address'], 'string'],
            [['birthday'], 'safe'],
            [['user_id'], 'string', 'max' => 15],
            [['password', 'name', 'college', 'major', 'class', 'person_id'], 'string', 'max' => 50],
            [['signature', 'email_t'], 'string', 'max' => 100],
            [['intro', 'native', 'politics'], 'string', 'max' => 250],
            [['tel'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'password' => 'Password',
            'name' => 'Name',
            'sex' => 'Sex',
            'signature' => 'Signature',
            'intro' => 'Intro',
            'skill' => 'Skill',
            'birthday' => 'Birthday',
            'college' => 'College',
            'major' => 'Major',
            'class' => 'Class',
            'native' => 'Native',
            'politics' => 'Politics',
            'address' => 'Address',
            'person_id' => 'Person ID',
            'tel' => 'Tel',
            'email_t' => 'Email T',
        ];
    }


    public function logout(){

    }

    /**教务绑定
     * @param $map  ['user_id'=学号,'password'=>密码]
     */
    public function bind(){
        $cookies=Yii::$app->request->cookies;//('JSESSIONID');die();
        //echo $cookies->get('JSESSIONID');die();

        $info['user_id'] = $this->user_id ; //$map['count'];
        $info['password'] = $this->password ;//$map['password'];
        $info['user_type'] = 'student';
        $info['set_language'] = 'cn';
        $info['ranstring'] = $this->ranstring;
        $cookie_file = $_POST['Student']['picId'];
        //echo $_POST['Student']['picId'];die();

        //$cookie_file = $info['picId'];//tempnam('./temp22','uu');   //创建临时文件保存cookie
        $login_url = 'http://jiaowu.swjtu.edu.cn/servlet/UserLoginSQLAction';//登陆地址
//$post_fields = '__VIEWSTATE=dDwtMTk3MjM2MzU0MDs7Po+Vuw2g98nkvMhqN2OzPbC6DnbA&TextBox1='.$username&TextBox2='.$password;//POST参数
        $ch = curl_init($login_url); //初始化
        curl_setopt($ch, CURLOPT_HEADER, 1); //0显示
        curl_setopt($ch, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //1不显示
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookie_file);   //保存cookie
        curl_setopt($ch, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookie_file);   //保存cookie

        curl_setopt($ch, CURLOPT_POST, 1);//POST数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($info));//加上POST变量
        $re = curl_exec($ch);
        curl_close($ch);

        $url2='http://jiaowu.swjtu.edu.cn/servlet/StudentInfoMapAction?MapID=101&PageUrl=../student/student/student.jsp';
        $ch2 = curl_init($url2);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch2, CURLOPT_COOKIE, $cookie_file);
        curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch2, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");

        curl_setopt($ch2, CURLOPT_AUTOREFERER,1);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_REFERER, 'http://jiaowu.swjtu.edu.cn/');
        $contents = curl_exec($ch2);  //执行并获取HTML文档内容
//iconv('GBK', 'UTF-8', $contents);//编码转换
        curl_close($ch2);
        //echo $contents;
        unlink($cookie_file);
        $preg = '/ffff\".*>&nbsp;.*<\/td>/';
        preg_match_all($preg,$contents,$infoList);
        //$info = [];
        function listInfo($c){
            $info = preg_split('/(>&nbsp;)|(<\/td>)/',$c);
            return $info[1];
        }
        foreach($infoList[0] as $k=>$v){
            //echo "<br>".$k;
            //$a2 = preg_split('/(>&nbsp;)|(<\/td>)/',$v);
            switch($k){
                case 0:$info['user_id'] = listInfo($v);break;
                case 1:$info['name'] = listInfo($v);break;
                case 3:$info['sex'] = listInfo($v);break;
                case 4:$info['birthday'] = listInfo($v);break;
                case 5:$info['college'] = listInfo($v);break;
                case 7:$info['major'] = listInfo($v);break;
                case 8:$info['class'] = listInfo($v);break;
                case 9:$info['native'] = listInfo($v);break;
                case 10:$info['nation'] = listInfo($v);break;
                case 11:$info['politics'] = listInfo($v);break;
                case 12:$info['person_id'] = listInfo($v);break;
                case 15:$info['tel'] = listInfo($v);break;
                case 16:$info['address'] = listInfo($v);break;
                default:break;
            }
        }

        if(empty($info['name'])){
            $this->attributes = $info;
            if(!$this->save()){
                //var_dump($this->getErrors());
            }
           /* echo $this->sex;
            var_dump($this->getErrors());
            echo '<br>';
            var_dump($this->attributes);
            //die();*/

            return  false;

        }else{
            /*if(!empty($map['code'])){
                $wechat = new Wechat();  //微信对象
                $wechatInfo = $wechat->getUserInfo($map['code']);   //获取用户微信信息
                $info['openid'] = $wechatInfo['openid'];
                $info['headimgurl'] = $wechatInfo['headimgurl'];
            }*/

            //$id = $this->add($info);
            //$info['uid'] = $id;
            //$info['power'] = $this->power;
            $this->attributes = $info;
            if(!$this->save()){
                return false;
                //var_dump($this->getErrors());
            }
            //var_dump($info);die();
            return true;   //返回给UindexModel
        }

       // return $re;

    }



    public function listView(&$list, $modelInfo, $module = 'admin')
    {
        $listExtra = implode(',',$modelInfo['list_extra'][$module]); //主内容附加项，如：user
        $reList =[];

        foreach($list as $k=>$v){
            if($this->mid==$v['model_id']) //选择对应模型
            {
                $raw = $this->query("select {$listExtra} from {$modelInfo['identity']} where id = {$v['uid']}");

                $reList[$k] = array_merge($v,$raw[0]);
                unset($list[$k]);
            }
        }
        return $reList;

    }

    /**
     * @param $url
     * @return array cookie文件名，pic图片路径
     */
    public function getRanstringPic(){

        $path = '.'. self::$RanstringPicPath ;  //保存路径
        $filename = time().rand(1,6000).'.jpeg';  // 'testqr.png';

        if (is_dir(basename($filename))) {
            echo "The Dir was not exits";
            Return false;
        }
        //$url = "http://jiaowu.swjtu.edu.cn/servlet/GetRandomNumberToJPEG".'?test='.time();//1490088525544;

        $url= self::$yzmUrl.'?test='.time();
        //去除URL连接上面可能的引号
        $url = preg_replace('/(?:^[\'"]+|[\'"\/]+$)/', '', $url);

        $cookie_file = tempnam('temp22','u');  //存储cookie文件

        $first = curl_init();  //第一次，获取cookie
        curl_setopt($first, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");
        curl_setopt($first, CURLOPT_URL, self::$loginPageUrl);
        curl_setopt($first, CURLOPT_HEADER, 1);
        curl_setopt($first,CURLOPT_RETURNTRANSFER,true);

        curl_setopt($first, CURLOPT_COOKIEJAR,  $cookie_file);
        curl_setopt($first, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($first, CURLOPT_COOKIEJAR,  $cookie_file); //保存cookie

        curl_setopt($first, CURLOPT_TIMEOUT, 30);
        curl_exec($first);
        curl_close($first);
        exec('chmod 777 '.$cookie_file);

        $hander = curl_init();    //第二次，获取图片
        curl_setopt($hander, CURLOPT_URL, $url);
        curl_setopt($hander, CURLOPT_REFERER, "http://jiaowu.swjtu.edu.cn/service/login.jsp?user_type=student");

        curl_setopt($hander, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($hander,CURLOPT_RETURNTRANSFER,true);

        //curl_setopt($hander, CURLOPT_COOKIEJAR,  $cookie_file); //保存cookie
        curl_setopt($hander, CURLOPT_COOKIEFILE,  $cookie_file);
        curl_setopt($hander, CURLOPT_COOKIEJAR,  $cookie_file); //保存cookie
        curl_setopt($hander, CURLOPT_TIMEOUT, 30);

        $output = curl_exec($hander);
        //curl_getinfo($hander);
        curl_close($hander);

        file_put_contents($path.$filename,$output);  //存储图片

        Return [ 'cookie'=>$cookie_file,'pic'=>self::$RanstringPicPath.$filename ];
    }
}
