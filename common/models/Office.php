<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property integer $id
 * @property string $office
 *
 */
class Office extends \yii\db\ActiveRecord
{

    const STATUS_AOLLOW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office'], 'string'],
            [['id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'office' => 'Office',
        ];
    }

    public static $STATUS_DELETE = 0;
    public static $STATUS_AOLLOW = 1;
    public static $STATUS_CANCLE = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAindices()
    {
        return $this->hasMany(Aindex::className(), ['urls' => 'id']);
    }
    public static function getUrlsName($id)
    {
        return self::findOne(['id'=>$id]);
    }
}
