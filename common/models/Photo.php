<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property string $savepath
 * @property string $savename
 * @property string $title
 * @property integer $room_id
 * @property string $createtime
 * @property integer $size
 * @property string $type
 * @property string $ext
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['savepath', 'savename', 'title', 'room_id', 'size', 'type', 'created_by'], 'required'],
            [['savename'], 'string'],
            [['room_id', ], 'integer'],
            [['size', ], 'double'],
            [['createtime'], 'safe'],
            [['savepath', 'title'], 'string', 'max' => 250],
            [['type',], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'savepath' => 'Savepath',
            'savename' => 'Savename',
            'title' => 'Title',
            'room_id' => 'Room ID',
            'createtime' => 'Createtime',
            'size' => 'Size',
            'type' => 'Type',
            'ext' => 'Ext',
        ];
    }
}
