<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;
use backend\models\Tag;

/**
 * This is the model class for table "information".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $level
 * @property integer $uri
 * @property string $create_time
 */
class Information extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content','uri'], 'string'],
            [['level'], 'integer'],
            [['create_time'], 'safe'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'create_time' => 'Create Time',
            'level' => 'Level',
            'uri' => 'Uri',
        ];
    }

}
