<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "facility".
 *
 * @property integer $room_id
 * @property string $child
 */
class Facility extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'child'], 'required'],
            [['room_id'], 'integer'],
            [['child'], 'string', 'max' => 50],
        ];
    }

    public $facility = [];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'child' => 'Child',
            'facility' => '设施',
        ];
    }
    public function roomFacilitySave($room_id){
        $this->room_id = $room_id;
        $arr = $this->facility;
       /* $d = Facility::findAll(['room_id'=>$room_id]);
        foreach($d as $a){
            $a::delete();
        }*/
        $c = new Facility();
        $c->room_id = $room_id;
        $c->deleteAll();
        if(is_array($arr)){
            foreach($arr as $v){
                //$this->room_id = $room_id;
                $f = new Facility();
                $f->child = $v;
                $f->room_id = $room_id;
                /* var_dump($this->child) ;
                 var_dump($this->room_id) ;
                 echo "<br>";*/
                //$this->load($v);
                $f->insert();
            }
        }


    }

}
