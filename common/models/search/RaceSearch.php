<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Race;

/**
 * RaceSearch represents the model behind the search form about `common\models\Race`.
 */
class RaceSearch extends Race
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'organization', 'leader', 'guider', 'introduce', 'rule', 'expecteddate', 'connect', 'website'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Race::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'organization', $this->organization])
            ->andFilterWhere(['like', 'leader', $this->leader])
            ->andFilterWhere(['like', 'guider', $this->guider])
            ->andFilterWhere(['like', 'introduce', $this->introduce])
            ->andFilterWhere(['like', 'rule', $this->rule])
            ->andFilterWhere(['like', 'expecteddate', $this->expecteddate])
            ->andFilterWhere(['like', 'connect', $this->connect])
            ->andFilterWhere(['like', 'website', $this->website]);

        return $dataProvider;
    }
}
