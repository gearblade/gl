<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student;

/**
 * StudentSearch represents the model behind the search form about `common\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['user_id', 'password', 'name', 'sex', 'signature', 'intro', 'skill', 'birthday', 'college', 'major', 'class', 'native', 'politics', 'address', 'person_id', 'tel', 'email_t'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
        ]);

        $query->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'signature', $this->signature])
            ->andFilterWhere(['like', 'intro', $this->intro])
            ->andFilterWhere(['like', 'skill', $this->skill])
            ->andFilterWhere(['like', 'college', $this->college])
            ->andFilterWhere(['like', 'major', $this->major])
            ->andFilterWhere(['like', 'class', $this->class])
            ->andFilterWhere(['like', 'native', $this->native])
            ->andFilterWhere(['like', 'politics', $this->politics])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'person_id', $this->person_id])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'email_t', $this->email_t]);

        return $dataProvider;
    }
}
