<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Mind;

/**
 * MindSearch represents the model behind the search form about `common\models\Mind`.
 */
class MindSearch extends Mind
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status', 'likes', 'oppose', 'cate','type'], 'integer'],
            [['title', 'content', 'create_at', 'img', 'icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type = null)
    {
        $query = Mind::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->type =$type;
        $this->load($params);
        //;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'create_at' => $this->create_at,
            'status' => $this->status,
            'likes' => $this->likes,
            'oppose' => $this->oppose,
            'cate' => $this->cate,
            'type' => $this->type,
            #'tag' => $this->tag,
        ]);
        //echo $this->type;die();

        $query->andFilterWhere(['like', 'title', $this->title])
            //->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        $query->orderBy(['create_at'=>SORT_DESC]);

        #$query->join('LEFT JOIN', 'document', 'document.mind_id = mind.id');

        return $dataProvider;
    }

    public function searchTag($params,$tags){
        $query = Mind::find();

        #echo $tags; die();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            /*'pagination' => [
                'pageSize' => 18,
            ],*/
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*$query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'create_at' => $this->create_at,
            'publish_at' => $this->publish_at,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->orFilterWhere(['like', 'publish', $this->publish])
            ->andFilterWhere(['like', 'author', $this->author])

            ->andFilterWhere(['like', 'edition', $this->edition])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'background', $this->background])
            ->andFilterWhere(['like', 'publish_num', $this->publish_num]);
        if(!empty($this->max)){
            $query->orFilterWhere(['like', 'name', $this->max])
                ->orFilterWhere(['like', 'author', $this->max])
                ->orFilterWhere(['like', 'publish', $this->max])
                ->orFilterWhere(['like', 'background', $this->max]);
        }*/
        foreach($tags as $tag){

            $query->where(['id'=>$tag->object_id]);
        }
        $query->orderBy(['create_at'=>SORT_DESC]);
        //echo $query->sql;die()

        return $dataProvider;
    }
}
