<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transdeal".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $type
 * @property float $weight
 * @property float $price
 * @property integer $path_id
 * @property integer $area_id
 * @property string $area
 * @property integer $destination_id
 * @property string $destination
 * @property integer $user_id
 * @property string $user_name
 * @property string $tel
 * @property integer $status
 * @property integer $distance
 * @property integer $current_node
 * @property integer $states
 * @property string $create_time
 */
class Transdeal extends \yii\db\ActiveRecord
{

    const STATUS_AOLLOW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transdeal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area', 'destination'], 'required'],
            [['id', 'type_id', 'path_id' ,'area_id', 'destination_id' ,'user_id' ,'status', 'current_node' ,'states','distance','weight','price'], 'integer'],
            [['type', 'area', 'destination', 'user_name', 'create_time', 'tel'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
            return [
            'id' => 'ID',
            'type' => 'Type',
            'user_name' => 'User',
            'area' => 'Area',
            'destination' => 'Destination',
            'distance' => 'Distance',
            'weight' => 'Weight',
            'price' => 'Price',
            // 'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public static $STATUS_DELETE = 0;
    public static $STATUS_AOLLOW = 1;
    public static $STATUS_CANCLE = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAindices()
    {
        return $this->hasMany(Aindex::className(), ['transdeal' => 'id']);
    }
    public static function getUrlsName($id)
    {
        return self::findOne(['id'=>$id]);
    }
}
