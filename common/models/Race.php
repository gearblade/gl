<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "race".
 *
 * @property integer $id
 * @property string $name
 * @property string $organization
 * @property string $leader
 * @property string $guider
 * @property string $introduce
 * @property string $rule
 * @property string $expecteddate
 * @property string $connect
 * @property string $website
 */
class Race extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'race';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['introduce', 'rule', 'website'], 'string'],
            [['name', 'organization'], 'string', 'max' => 250],
            [['leader'], 'string', 'max' => 50],
            [['guider'], 'string', 'max' => 200],
            [['expecteddate', 'connect'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '竞赛名称',
            'organization' => '承办学院',
            'leader' => '领队老师',
            'guider' => '指导老师',
            'introduce' => '大赛简介',
            'rule' => '赛事规则',
            'expecteddate' => '竞赛时间',
            'connect' => '联系方式',
            'website' => '官方网址',
        ];
    }
}
