<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property integer $mind_id
 * @property string $uname
 * @property string $content
 * @property string $race
 * @property integer $limit_member
 * @property integer $already_member
 * @property string $connect
 * @property string $orienting
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mind_id',   ], 'required'],
            [['mind_id', 'limit_member', 'already_member'], 'integer'],
            [['content', 'connect'], 'string'],
            [['uname'], 'string', 'max' => 50],
            [['race'], 'string', 'max' => 100],
            [['orienting'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mind_id' => 'Mind ID',
            'uname' => 'Uname',
            'content' => '描述',
            'race' => '比赛',
            'limit_member' => '人数',
            'already_member' => '已有人数',
            'connect' => '联系方式',
            'orienting' => '面向',
        ];
    }
}
