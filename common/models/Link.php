<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;
use backend\models\Tag;

/**
 * This is the model class for table "link".
 *
 * @property integer $id
 * @property string $title
 * @property string $uri
 * @property string $icon
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'uri'], 'required'],
            [['uri','icon'], 'string'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'uri' => 'Uri',
            'icon' => 'Icon',
        ];
    }
}
