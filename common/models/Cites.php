<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cites".
 *
 * @property integer $id
 * @property string $cityname
 *
 */
class Cites extends \yii\db\ActiveRecord
{

    const STATUS_AOLLOW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityname'], 'string'],
            [['id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cityname' => 'CityName',
        ];
    }

    public static $STATUS_DELETE = 0;
    public static $STATUS_AOLLOW = 1;
    public static $STATUS_CANCLE = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAindices()
    {
        return $this->hasMany(Aindex::className(), ['urls' => 'id']);
    }
    public static function getUrlsName($id)
    {
        return self::findOne(['id'=>$id]);
    }
}
