<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transpath2".
 *
 * @property integer $id
 * @property string $area
 * @property integer $area_id
 * @property string $destination
 * @property integer $destination_id
 * @property integer $distance
 * @property integer $price
 * @property float $rate
 * @property string $office
 * @property integer $office_id
 *
 */
class Transpath2 extends \yii\db\ActiveRecord
{

    const STATUS_AOLLOW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transpath2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area', 'destination'], 'required'],
            [['distance', 'price', 'area_id' ,'destination_id'], 'integer'],
            [['area', 'destination'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
            return [
            'id' => 'ID',
            'area' => 'Area',
            'area_id' => 'Area id',
            'destination' => 'Destination',
            'distance' => 'Distance',
            'destination_id' => 'Destination id',
            'price' => 'Price',
            'rate' => 'Rate',
            'office' => 'Office',
            'office_id' => 'Office id',
        ];
    }

    public static $STATUS_DELETE = 0;
    public static $STATUS_AOLLOW = 1;
    public static $STATUS_CANCLE = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAindices()
    {
        return $this->hasMany(Aindex::className(), ['transpath2' => 'id']);
    }
    public static function getUrlsName($id)
    {
        return self::findOne(['id'=>$id]);
    }
}
