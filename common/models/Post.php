<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $content
 * @property string $pic1
 * @property string $pic2
 * @property integer $mind_id
 * @property integer $post_id
 * @property string $create_at
 * @property integer $user_id
 * @property integer $unname
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    const SCENARIO_ANSWER = 'answer';
    const SCENARIO_COMMIT = 'commit';

    public function scenarios()
    {
        return [
            'default'=>['content', 'mind_id', 'user_id'],
            self::SCENARIO_ANSWER => ['content', 'mind_id', 'user_id'],
            self::SCENARIO_COMMIT => ['content', 'post_id', 'user_id','post_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content',  'user_id'], 'required'],
            [['content', 'pic1', 'pic2'], 'string'],
            [['mind_id','post_id', 'user_id','unname','post_id'], 'integer'],
            [['create_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'pic1' => 'Pic1',
            'pic2' => 'Pic2',
            'mind_id' => 'Post ID',
            'create_at' => 'Create At',
            'user_id' => 'User ID',
            'unname' => '匿名',
        ];
    }
    /*public function getUsername(){
        return User::findOne(['id'=>$this->user_id])->username;
    }*/
    public function getUser(){
        return User::findOne(['id'=>$this->user_id]);
    }
    public function getCommentCount(){
        return $this->find()->where(['post_id'=>$this->id])->count();
    }
}
